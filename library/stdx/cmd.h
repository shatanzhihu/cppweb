#ifndef XG_CMD_H
#define XG_CMD_H
/////////////////////////////////////////////////////
#include "File.h"

namespace proc
{
	int argc();
	const char* argv(int index);
	const char* argv(const char* key);
	Process* init(int argc, char** argv);

	string pwd();
	string pwd(const string& path);
	string system(const string& cmd);

	string home();
	string exepath();
	string exename();
	string appname();
	string env(const string& key);
	string env(const string& key, const string& val);

	long tid();
	void daemon();
	PROCESS_T pid();
	void exit(int code = 0);
	void wait(PROCESS_T pid);
	void kill(PROCESS_T pid, int flag = 0);

	vector<ProcessData> ps();
	vector<ProcessData> ps(string name);
};

namespace path
{
	string name(const string& path);
	string parent(const string& path);
	string extname(const string& path);

	string cat(const string& path);
	sp<XFile> create(const string& path, bool force = false);
	sp<XFile> open(const string& path, E_OPEN_MODE mode = eWRITE);

	long size(const string& path);
	bool mkdir(const string& path);
	bool isdir(const string& path);
	bool isfile(const string& path);
	bool exists(const string& path);
	bool remove(const string& path);
	time_t mtime(const string& path);
	time_t mtime(const string& path, const time_t& tm);
	vector<string> dir(const string& path, bool file = true);
	vector<string> find(const string& path, const string& filter);
};

namespace cmdx
{
	bool CheckCommand(const char* fmt, ...);
	void PrintHexData(const void* msg, int len);
	int SelectCommand(const vector<string>& menus, const char* msg = NULL);
};
/////////////////////////////////////////////////////
#endif