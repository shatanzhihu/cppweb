#ifndef XG_CMD_CPP
#define XG_CMD_CPP
////////////////////////////////////////////////////////
#include "../cmd.h"
#include "../../clib/utils.h"

bool cmdx::CheckCommand(const char* fmt, ...)
{
	int n;
	string str;
	va_list args;

	str.clear();
	va_start(args, fmt);
	n = stdx::vformat(str, fmt, args);
	va_end(args);

	printf("%s", str.c_str());

	while (true)
	{
		n = getch();

		if (n == 'Y' || n == 'y')
		{
			SetConsoleTextColor(eGREEN);
			printf(" (YES)\n");
			SetConsoleTextColor(eWHITE);

			return true;
		}
		else if (n == 'N' || n == 'n')
		{
			SetConsoleTextColor(eRED);
			printf(" (NO)\n");
			SetConsoleTextColor(eWHITE);

			return false;
		}
	}

	return false;
}

void cmdx::PrintHexData(const void* msg, int len)
{
	int i = 0;
	int n = 0;
	u_char buffer[32] = {0};
	u_char* data = (u_char*)(msg);
	
	for (i = 0; i < len; i++)
	{
		if (i % 16 == 0)
		{
			if (n == 16)
			{
				n = 0;
				SetConsoleTextColor(eGREEN);
				printf(" %s\n", buffer);
				SetConsoleTextColor(eWHITE);
				memset(buffer, 0, sizeof(buffer));
			}
			
			SetConsoleTextColor(eYELLOW);
			printf("[%04X] ", i);
			SetConsoleTextColor(eWHITE);
		}

		printf("%02X ", data[i] % 256);
		
		buffer[n++] = (data[i] > 0X1F && data[i] < 0X7F) ? data[i] : '.';
	}
	
	if (n > 0)
	{
		for (i = n; i < 16; i++) printf("   ");	

		SetConsoleTextColor(eGREEN);
		printf(" %s\n", buffer);
		SetConsoleTextColor(eWHITE);
	}
}

int cmdx::SelectCommand(const vector<string>& menus, const char* msg)
{
	string str;
	int ch = 0;
	int cnt = menus.size();


	if (msg && *msg)
	{
		printf(" %s\n", msg);
		puts("-----------------------------------------");
	}

	for (int i = 0; i < cnt; i++)
	{
		printf(" %d.%s\n", i + 1, menus[i].c_str());
	}

	puts("------------------------------------------");
	printf(" press numeric key to select the menu : ");

	while (true)
	{
		ch = getch();
		ch -= '0';

		if (ch >= 1 && ch <= cnt)
		{
			printf("%d\n", ch);

			return ch;
		}
	}

	return 0;
}


int proc::argc()
{
	return Process::GetCmdParamCount();
}
const char* proc::argv(int index)
{
	return Process::GetCmdParam(index);
}
const char* proc::argv(const char* key)
{
	return Process::GetCmdParam(key);
}
Process* proc::init(int argc, char** argv)
{
	return Process::Instance(argc, argv);
}

string proc::pwd()
{
	return Process::GetCurrentDirectory();
}
string proc::pwd(const string& path)
{
	Process::SetCurrentDirectory(path);

	return pwd();
}
string proc::system(const string& cmd)
{
	SmartBuffer buffer(1024 * 1024);

	if (RunCommand(cmd.c_str(), buffer.str(), buffer.size(), true) >= 0) return buffer.str();

	return stdx::EmptyString();
}

string proc::home()
{
	return Process::GetEnv("HOME");
}
string proc::exepath()
{
	string path;

	Process::GetProcessExePath(path);

	return std::move(path);
}
string proc::exename()
{
	return stdx::GetFileNameFromPath(exepath());
}
string proc::appname()
{
	return Process::GetAppname();
}
string proc::env(const string& key)
{
	return Process::GetEnv(key);
}
string proc::env(const string& key, const string& val)
{
	Process::SetEnv(key, val);

	return Process::GetEnv(key);
}

long proc::tid()
{
	return GetCurrentThreadId();
}
void proc::daemon()
{
	Process::InitDaemon();
}
PROCESS_T proc::pid()
{
	return Process::GetCurrentProcess();
}
void proc::exit(int code)
{
	SystemExit(code);
}
void proc::wait(PROCESS_T pid)
{
	Process::Wait(pid);
}
void proc::kill(PROCESS_T pid, int flag)
{
	Process::Kill(pid, flag);
}

vector<ProcessData> proc::ps()
{
	vector<ProcessData> vec;

	Process::GetSystemProcessList(vec);

	return std::move(vec);
}
vector<ProcessData> proc::ps(string name)
{
	vector<ProcessData> vec;

	Process::GetSystemProcessListByName(vec, name);

	return std::move(vec);
}

string path::name(const string& path)
{
	return stdx::GetFileNameFromPath(path);
}
string path::parent(const string& path)
{
	return stdx::GetParentPath(path);
}
string path::extname(const string& path)
{
	return stdx::GetExtNameFromPath(path);
}

string path::cat(const string& path)
{
	string content;

	stdx::GetFileContent(content, path);

	return std::move(content);
}
sp<XFile> path::create(const string& path, bool force)
{
	int flag = GetPathType(path.c_str());

	if (force)
	{
		if (flag == ePATH) return NULL;
	}
	else
	{
		if (flag >= ePATH) return NULL;
	}

	sp<XFile> file = newsp<XFile>();

	return file->open(path, eCREATE) ? file : sp<XFile>();
}
sp<XFile> path::open(const string& path, E_OPEN_MODE mode)
{
	sp<XFile> file = newsp<XFile>();

	return file->open(path, mode) ? file : sp<XFile>();
}

long path::size(const string& path)
{
	return GetFileLength(path.c_str());
}
bool path::mkdir(const string& path)
{
	return CreateFolder(path.c_str()) >= 0;
}
bool path::isdir(const string& path)
{
	return GetPathType(path.c_str()) == ePATH;
}
bool path::isfile(const string& path)
{
	return GetPathType(path.c_str()) == eFILE;
}
bool path::exists(const string& path)
{
	return GetPathType(path.c_str()) >= ePATH;
}
bool path::remove(const string& path)
{
	return RemoveFolder(path.c_str()) >= 0;
}
time_t path::mtime(const string& path)
{
	return GetFileUpdateTime(path.c_str());
}
time_t path::mtime(const string& path, const time_t& tm)
{
	SetFileUpdateTime(path.c_str(), &tm);

	return GetFileUpdateTime(path.c_str());
}
vector<string> path::dir(const string& path, bool file)
{
	vector<string> vec;

	stdx::GetFolderContent(vec, path, file ? eFILE : ePATH);

	return std::move(vec);
}
vector<string> path::find(const string& path, const string& filter)
{
	vector<string> vec;

	stdx::FindFile(vec, path, filter);

	return std::move(vec);
}
////////////////////////////////////////////////////////
#endif
