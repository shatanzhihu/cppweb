#ifndef XG_DBCONNECT_CPP
#define XG_DBCONNECT_CPP
//////////////////////////////////////////////////////////////
#include "../DBConnect.h"

static string GetBindString(const string& sysname)
{
	if (sysname == "Oracle")
	{
		string str;
		thread_local int idx = 0;

		if (++idx > 9999) idx = 1;

		stdx::format(str, ":%d", idx);

		return str;
	}

	return "?";
}

string DBData::toValueString(const string& sysname) const
{
	switch(type())
	{
		case DBData_Blob: return GetBindString(sysname);
		case DBData_String: return GetBindString(sysname);
		case DBData_DateTime:
			if (none) return "NULL";
			if (sysname == "MySQL") return stdx::format("str_to_date('%s','%%Y-%%m-%%d %%H:%%i:%%s')", toString().c_str());
			if (sysname == "Oracle") return stdx::format("to_timestamp('%s','YYYY-MM-DD HH24:MI:SS')", toString().c_str());
			return stdx::format("'%s'", toString().c_str());
		default: return none ? "NULL" : toString();
	}
}

const char* ColumnData::getTypeString() const
{
	switch(type)
	{
		case DBData_Blob: return "DBBlob";
		case DBData_Float: return "DBFloat";
		case DBData_Integer: return "DBInteger";
		case DBData_DateTime: return "DBDateTime";
		default: return "DBString";
	}
}

int RowData::getInt(int index)
{
	return (int)getLong(index);
}
float RowData::getFloat(int index)
{
	return (float)getDouble(index);
}
double RowData::getDouble(int index)
{
	return stdx::atof(getString(index).c_str());
}
long long RowData::getLong(int index)
{
	return stdx::atol(getString(index).c_str());
}
SmartBuffer RowData::getBinary(int index)
{
	SmartBuffer buffer;

	int len = getDataLength(index);

	if (len <= 0 || isNull()) return buffer;

	len = getData(index, (char*)buffer.malloc(len), len);

	if (len <= 0) buffer.free();

	return buffer;
}
DateTime RowData::getDateTime(int index)
{
	DateTime datetime;

	getDateTime(datetime, index);

	return std::move(datetime);
}
bool RowData::getDateTime(DateTime& datetime, int index)
{
	datetime = DateTime::FromString(getString(index));

	return datetime.canUse();
}
char* RowData::getString(int index, char* data, int len)
{
	string str = getString(index);

	strncpy(data, str.c_str(), len);
	data[len - 1] = 0;

	return data;
}

string DBConnect::getBindString()
{
	return GetBindString(getSystemName());
}
int DBConnect::exec(const char* sql)
{
	static vector<DBData*> vec;

	return execute(sql, vec);
}
bool DBConnect::begin(bool commited)
{
	if (commited) CHECK_FALSE_RETURN(commit());

	return execute("BEGIN") >= SQLRtn_Duplicate;
}
int DBConnect::release(sp<QueryResult>& rs)
{
	rs = NULL;

	return 0;
}
bool DBConnect::setCharset(const string& charset)
{
	return true;
}
sp<QueryResult> DBConnect::quickQuery(const string& sql)
{
	return query(sql);
}
int DBConnect::bind(void* stmt, const vector<DBData*>& vec)
{
	return 0;
}
int DBConnect::updateStatus(int rowcnt, const string& sqlcmd, const vector<DBData*>& paramlist)
{
	if (status.update(this, rowcnt, sqlcmd, paramlist) < 0)
	{
		if (loglevel <= eERR) LogTrace(eERR, status.toString());
	}
	else
	{
		if (loglevel <= eTIP) LogTrace(eTIP, status.toString());
	}

	return rowcnt;
}

string DBConnect::Status::toString() const
{
	string res;

	if (rowcnt < 0)
	{
		if (errcode)
		{
			stdx::append(res, "execute sqlcmd[%s] failed[%d][%s]", sqlcmd.c_str(), errcode, errmsg.c_str());
		}
		else
		{
			stdx::append(res, "execute sqlcmd[%s] failed[%d]", sqlcmd.c_str(), rowcnt);
		}
	}
	else
	{
		stdx::append(res, "execute sqlcmd[%s] success[%d]", sqlcmd.c_str(), rowcnt);
	}

	if (paramlist.empty()) return res;

	res += " with param";

	for (const string& item : paramlist) stdx::append(res, "[%s]", item.c_str());

	return res;
}
int DBConnect::Status::update(DBConnect* conn, int rowcnt, const string& sqlcmd, const vector<DBData*>& paramlist)
{
	this->rowcnt = rowcnt;
	this->sqlcmd = sqlcmd;
	this->errcode = conn->getErrorCode();
	this->errmsg = conn->getErrorString();

	this->paramlist.clear();

	for (DBData* item : paramlist) this->paramlist.push_back(item->toString());

	return rowcnt;
}
//////////////////////////////////////////////////////////////
#endif