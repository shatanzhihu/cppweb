#ifndef XG_HEADER_UTILS
#define XG_HEADER_UTILS
//////////////////////////////////////////////////////////
#include "typedef.h"


#ifndef XG_CHARVALUE_BUFLEN
#define XG_CHARVALUE_BUFLEN		256
#endif

#define ColorPrint(__COLOR__, __FMT__, ...)		\
SetConsoleTextColor(__COLOR__);					\
printf(__FMT__, __VA_ARGS__);					\
SetConsoleTextColor(eWHITE);

#ifdef __cplusplus
extern "C" {
#endif
	typedef struct
	{
		int hour, min, sec;
		int year, month, yday, wday;

		union
		{
			int day;
			int mday;
		};
	} stDateTime;

	typedef struct
	{
		char val[XG_CHARVALUE_BUFLEN];
	} stCharBuffer;

	typedef void(*VOID_FUNC)();
	typedef BOOL(*CHECK_PASSWORD_FUNC)(char ch);

	inline static BOOL CheckSystemSizeof()
	{
		return sizeof(int8) == 1 && sizeof(u_int8) == 1
			&& sizeof(int16) == 2 && sizeof(u_int16) == 2
			&& sizeof(int32) == 4 && sizeof(u_int32) == 4
			&& sizeof(int64) == 8 && sizeof(u_int64) == 8;
	}

	inline static BOOL HandleCanUse(HANDLE handle)
	{
#ifdef XG_LINUX
		return handle >= 0;
#else
		return handle && handle != INVALID_HANDLE_VALUE;
#endif
	}

#ifdef XG_LINUX
	int getch();
#else
	BOOL usleep(unsigned long usec);
#endif

	BOOL DaemonInit();

	BOOL ClearConsole();

	void SystemExit(int code);

	BOOL IsSmallEndianSystem();

	unsigned long long GetTime();

	const char* GetCurrentUser();

	const char* GetLocaleCharset();
	
	void SystemPause(const char* msg);
	
	BOOL IsAlphaString(const char* str);

	BOOL IsAlnumString(const char* str);

	int GetHostInteger(const char* host);

	BOOL IsNumberString(const char* str);

	BOOL IsAmountString(const char* str);

	long GetFileLength(const char* path);

	time_t GetFileUpdateTime(const char* path);

	char* TrimString(char* str, const char* space);

	BOOL SetConsoleTextColor(E_CONSOLE_COLOR color);

	BOOL GetDateTime(stDateTime* dt, const time_t* tm);

	int GetStringCount(const char* str, const char* src);

	BOOL SetFileUpdateTime(const char* path, const time_t* tm);

	stCharBuffer GetTrimString(const char* str, const char* space);

	const char* SkipStartString(const char* str, const char* space);

	int RunCommand(const char* cmd, char* buffer, int sz, BOOL hidewnd);
	
	char* GetLineValue(char* line, const char* tag, const char* spliter);

	void ReadPassword(char* str, int len, char echo, CHECK_PASSWORD_FUNC func);

	stCharBuffer GetTruncatedString(const char* str, int len, const char* space);
#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////
#endif