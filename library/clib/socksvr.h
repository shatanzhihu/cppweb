#ifndef XG_HEADER_SOCKSVR
#define XG_HEADER_SOCKSVR
////////////////////////////////////////////////////////////////////
#include "../clib/system.h"

#ifdef __cplusplus
extern "C" {
#endif
	typedef struct
	{
		char flag[8];
		char addr[32];
	} stConnectData;

	BOOL ServerSocketAttach(SOCKET sock, const char* address);

	void ServerSocketLoop(const char* host, int port, int backlog, int timeout);

	void ServerSocketSetLockFunction(void(*lock)(), void(*unlock)());

	void ServerSocketSetProcessFunction(int(*func)(SOCKET, stConnectData*));

	void ServerSocketSetConnectClosedFunction(int(*func)(SOCKET, stConnectData*));
	
	void ServerSocketSetConnectFunction(int(*func)(SOCKET, stConnectData*, const char*, int));
#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////
#endif
