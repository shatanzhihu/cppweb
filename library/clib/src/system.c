#ifdef XG_LINUX

#include <netdb.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/statfs.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>

#define ioctlsocket ioctl

#ifndef __USE_LARGEFILE64
#define __USE_LARGEFILE64
#endif

#ifndef __USE_FILE_OFFSET64
#define __USE_FILE_OFFSET64
#endif

#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#endif

#include "../system.h"

#ifdef _MSC_VER
#pragma comment(lib, "WS2_32.lib")
#endif


int RemoveFile(const char* path)
{
	return remove(path);
}

int CreateNewFile(const char* path)
{
	FILE* fp = NULL;
	int flag = GetPathType(path);

	if (flag) return flag;

	if ((fp = fopen(path, "wb+")) == NULL) return XG_IOERR;

	fclose(fp);

	return 0;
}

int RemoveFolder(const char* path)
{
	char buffer[MAX_PATH + MAX_PATH] = {0};
	
#ifdef XG_LINUX
	snprintf(buffer, sizeof(buffer), "rm -rf \"%s\" > /dev/null 2>&1", path);
#else
	snprintf(buffer, sizeof(buffer), "RD /S /Q \"%s\"", path);
#endif
	
	return RunCommand(buffer, NULL, 0, TRUE);
}

int CreateFolder(const char* path)
{
	int i = 0;
	int len = strlen(path);
	char szDirName[MAX_PATH] = {0};

	if (len >= MAX_PATH) return XG_PARAMERR;
	if (GetPathType(path) == ePATH) return XG_OK;

	if (path[len - 1] == '/')
	{
		memcpy(szDirName, path, len);
	}
	else
	{
		memcpy(szDirName, path, len);

		szDirName[len++] = '/';
	}

	for (i = 1; i < len; i++)
	{
		if (szDirName[i] == '/')
		{
			szDirName[i] = 0;
#ifdef XG_LINUX
			if (access(szDirName, F_OK))
			{
				if (mkdir(szDirName, 0755) < 0) return XG_ERROR;
			}
#else
			if (GetPathType(szDirName) == eNONE) CreateDirectoryA(szDirName, NULL);
#endif
			szDirName[i] = '/';
		}
	}

	return 0;
}

int CloneFile(const char* src, const char* dest)
{
	int len = 0;
	long sum = 0;
	HANDLE srcfile;
	HANDLE destfile;

	if ((sum = GetFileLength(src)) >= 0 && HandleCanUse(srcfile = Open(src, eREAD)))
	{
		RemoveFolder(dest);

		if (HandleCanUse(destfile = Open(dest, eCREATE)))
		{
			long readed = 0;
			char buffer[64 * 1024];

			while ((len = sum - readed) > 0)
			{
				if (len > sizeof(buffer)) len = sizeof(buffer);
				if ((len = Read(srcfile, buffer, len)) <= 0) break;
				if ((len = Write(destfile, buffer, len)) <= 0) break;

				readed += len;
			}

			Close(destfile);
		}

		Close(srcfile);
	}

	return len < 0 ? len : sum; 
}

#ifdef XG_LINUX

#ifndef O_LARGEFILE
#define O_LARGEFILE	0
#endif

HANDLE Open(const char* filename, E_OPEN_MODE mode)
{
	if (GetPathType(filename) == ePATH) return INVALID_HANDLE_VALUE;

	if (mode == eCREATE) return open(filename, O_RDWR | O_LARGEFILE | O_CREAT | O_TRUNC, XG_IPC_FLAG);

	if (mode == eREAD) return open(filename, O_RDONLY | O_LARGEFILE);

	return open(filename, O_RDWR | O_LARGEFILE);
}

void Close(HANDLE handle)
{
	close(handle);
}

long long Seek(HANDLE handle, long long offset, int mode)
{
	return lseek64(handle, offset, mode);
}

int Read(HANDLE handle, void* data, int size)
{
	return read(handle, data, size);
}

int Write(HANDLE handle, const void* data, int size)
{
	return write(handle, data, size);
}

int GetPathType(const char* path)
{
	struct stat info;

	if (stat(path, &info) < 0) return eNONE;

	return S_ISDIR(info.st_mode) ? ePATH : eFILE;
}

BOOL SetPathAttributes(const char* path, E_OPEN_MODE mode)
{
	if (eREAD == mode) return (chmod(path, S_IRUSR) >= 0) ? TRUE : FALSE;

	if (eWRITE == mode) return (chmod(path, S_IRUSR | S_IWUSR) >= 0) ? TRUE : FALSE;

	return FALSE;
}

#else

HANDLE Open(const char* filename, E_OPEN_MODE mode)
{
	if (GetPathType(filename) == ePATH) return INVALID_HANDLE_VALUE;

	if (mode == eREAD) return CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	return CreateFileA(filename, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, mode == eCREATE ? CREATE_ALWAYS : OPEN_EXISTING, 0, NULL);
}

void Close(HANDLE handle)
{
	CloseHandle(handle);
}

long long Seek(HANDLE handle, long long offset, int mode)
{
	LARGE_INTEGER large;

	memset(&large, 0, sizeof(LARGE_INTEGER));

	large.QuadPart = offset;

	if (SetFilePointerEx(handle, large, &large, mode)) return large.QuadPart;

	return XG_ERROR;
}

int Read(HANDLE handle, void* data, int size)
{
	DWORD readed = 0;

	if (ReadFile(handle, data, (DWORD)(size), &readed, NULL)) return readed;

	return XG_ERROR;
}

int Write(HANDLE handle, const void* data, int size)
{
	DWORD writed = 0;

	if (WriteFile(handle, data, (DWORD)(size), &writed, NULL)) return writed;

	return XG_ERROR;
}

int GetPathType(const char* path)
{
	DWORD flag = GetFileAttributesA(path);

	if (flag == INVALID_FILE_ATTRIBUTES) return eNONE;

	return (flag & FILE_ATTRIBUTE_DIRECTORY) ? ePATH : eFILE;
}

BOOL SetPathAttributes(const char* path, E_OPEN_MODE mode)
{
	DWORD flag = 0;

	if (mode == eREAD)
	{
		flag = FILE_ATTRIBUTE_READONLY;
	}
	else if (mode == eWRITE)
	{
		flag = FILE_ATTRIBUTE_NORMAL;
	}
	else if (mode == eHIDDEN)
	{
		flag = GetFileAttributesA(path);

		if (flag == INVALID_FILE_ATTRIBUTES) return FALSE;

		flag |= FILE_ATTRIBUTE_HIDDEN;
	}
	else if (mode == eVISIBLE)
	{
		flag = GetFileAttributesA(path);

		if (flag == INVALID_FILE_ATTRIBUTES) return FALSE;

		if ((flag & FILE_ATTRIBUTE_HIDDEN) == 0) return TRUE;

		flag ^= FILE_ATTRIBUTE_HIDDEN;
	}

	return SetFileAttributesA(path, flag);
}

#endif

long long Tell(HANDLE handle)
{
	return Seek(handle, 0, SEEK_CUR);
}

void Flush(HANDLE handle)
{
#ifndef XG_LINUX
	FlushFileBuffers(handle);
#endif
}

int GetCpuCount()
{
	static int num = 0;

	if (num == 0)
	{
#ifdef XG_LINUX
		num = get_nprocs();
#else
		SYSTEM_INFO info;
		GetSystemInfo(&info);
		num = info.dwNumberOfProcessors;
#endif
	}

	return num;
}

BOOL GetCpuTime(long long* usr, long long* idle, long long* kernel)
{
#ifdef XG_LINUX
	FILE* fp = fopen("/proc/stat", "r");

	CHECK_FALSE_RETURN(fp);

	char tmp[1024] = {0};
	char buffer[1024] = {0};
	long long val = fread(buffer, sizeof(buffer) - 128, sizeof(char), fp);

	fclose(fp);
	strcat(buffer, " 0 0 0 0 0 0 ");
	sscanf(buffer, "%s %lld %lld %lld %lld", tmp, usr, &val, kernel, idle);

	*kernel += *idle + val;
#else
	FILETIME usrtime;
	FILETIME idletime;
	FILETIME kerneltime;

	CHECK_FALSE_RETURN(GetSystemTimes(&idletime, &kerneltime, &usrtime));

	*usr = (((long long)(usrtime.dwHighDateTime) << 32) | usrtime.dwLowDateTime) / 10;
	*idle = (((long long)(idletime.dwHighDateTime) << 32) | idletime.dwLowDateTime) / 10;
	*kernel = (((long long)(kerneltime.dwHighDateTime) << 32) | kerneltime.dwLowDateTime) / 10;
#endif
	return TRUE;
}

BOOL GetDiskSpace(const char* path, long long* total, long long* avail, long long* free)
{
#ifdef XG_LINUX
	struct statfs stat;

	if (path == NULL) path = "/";

	if (statfs(path, &stat) < 0) return FALSE;

	*free = stat.f_bfree * stat.f_bsize;
	*total = stat.f_blocks * stat.f_bsize;
	*avail = stat.f_bavail * stat.f_bsize;

	return TRUE;
#else
	if (path == NULL)
	{
		int num = 0;
		char str[] = "X:/";
		long long a = 0, t = 0, f = 0;

		*free = 0;
		*avail = 0;
		*total = 0;

		for (char ch = 'A'; ch <= 'Z'; ch++)
		{
			*str = ch;

			if (GetDiskFreeSpaceExA(str, (PULARGE_INTEGER)(&a), (PULARGE_INTEGER)(&t), (PULARGE_INTEGER)(&f)))
			{
				num = 0;
				*free += f;
				*avail += a;
				*total += t;
			}
			else
			{
				if (++num > 4) break;
			}
		}

		return *total > 0 ? TRUE : FALSE;
	}

	return GetDiskFreeSpaceExA(path, (PULARGE_INTEGER)(avail), (PULARGE_INTEGER)(total), (PULARGE_INTEGER)(free));
#endif
}

int CalcCpuUseage(int delay)
{
	long long usr, idle, kernel;
	long long preusr, preidle, prekernel;

	GetCpuTime(&preusr, &preidle, &prekernel);

	Sleep(delay);

	GetCpuTime(&usr, &idle, &kernel);

	usr -= preusr;
	idle -= preidle;
	kernel -= prekernel;

	return kernel + usr > 0 ? (kernel + usr - idle) * 100 / (kernel + usr) : 0;
}

BOOL GetMemoryInfo(long long* total, long long* avail)
{
#ifdef XG_LINUX
	struct sysinfo info;

	CHECK_FALSE_RETURN(sysinfo(&info) >= 0);

	if (total) *total = info.totalram;
	if (avail) *avail = info.freeram;
#else
	MEMORYSTATUSEX info;

	info.dwLength = sizeof(MEMORYSTATUSEX);

	CHECK_FALSE_RETURN(GlobalMemoryStatusEx(&info));

	if (total) *total = info.ullTotalPhys;
	if (avail) *avail = info.ullAvailPhys;
#endif
	return TRUE;
}

BOOL ReadSector(HANDLE handle, void* data, long long offset, long long num)
{
	offset *= 512;

	CHECK_FALSE_RETURN(Seek(handle, offset, SEEK_SET) == offset);

	num *= 512;

	return Read(handle, data, num) == num ? TRUE : FALSE;
}

BOOL WriteSector(HANDLE handle, const void* data, long long offset, long long num)
{
	offset *= 512;

	CHECK_FALSE_RETURN(Seek(handle, offset, SEEK_SET) == offset);

	num *= 512;

	return Write(handle, data, num) == num ? TRUE : FALSE;
}

HANDLE OpenDisk(int diskno, E_OPEN_MODE mode)
{
#ifdef XG_LINUX
	char str[] = "/dev/sd#";

	diskno += 'a';

	if (diskno > 'z') return (HANDLE)(-1);

	str[strlen(str) - 1] = (char)(diskno);
#else
	char str[] = "\\\\.\\PHYSICALDRIVE##";

	size_t len = strlen(str);

	if (diskno < 10)
	{
		str[len - 2] = (char)('0' + diskno);
		str[len - 1] = 0;
	}
	else if (diskno < 100)
	{
		str[len - 2] = (char)('0' + diskno / 10);
		str[len - 1] = (char)('0' + diskno % 10);
	}
	else
	{
		return (HANDLE)(-1);
	}
#endif

	return Open(str, mode);
}

HANDLE OpenPartition(int ch, E_OPEN_MODE mode)
{
#ifdef XG_LINUX
	char str[] = "/dev/sda#";

	ch -= (ch > 'Z') ? 'a' : 'A';

	if (ch > 10) return (HANDLE)(-1);

	str[strlen(str) - 1] = (char)(ch + '1');
#else
	char str[] = "\\\\.\\#:";

	if (ch > 'Z') ch -= 'z' - 'Z';

	str[strlen(str) - 2] = (char)(ch);
#endif

	return Open(str, mode);
}

BOOL IsSocketTimeout()
{
#ifdef XG_LINUX
	return errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR;
#else
	return WSAGetLastError() == WSAETIMEDOUT;
#endif
}

void SocketClose(SOCKET sock)
{
	if (IsSocketClosed(sock)) return;

#ifdef XG_LINUX
	close(sock);
#else
	closesocket(sock);
#endif
}

BOOL IsSocketClosed(SOCKET sock)
{
	return sock == INVALID_SOCKET || sock < 0;
}

int GetLocalAddress(char* host[])
{
	char hostname[256];

	if (gethostname(hostname, sizeof(hostname)) < 0) return XG_SYSERR;

	int num = 0;
	struct hostent* data = gethostbyname(hostname);

	if (host == NULL) return XG_SYSERR;

	while (num < 32 && data->h_addr_list[num])
	{
		host[num] = inet_ntoa(*(struct in_addr*)(data->h_addr_list[num]));

		num++;
	}

	return num;
}

BOOL IsLocalHost(const char* host)
{
	int cnt;
	char* vec[32];

	if (strcmp(host, LOCAL_IP) == 0) return TRUE;

	cnt = GetLocalAddress(vec);

	while (--cnt >= 0)
	{
		if (strcmp(host, vec[cnt]) == 0) return TRUE;
	}

	return FALSE;
}

SOCKET SocketConnect(const char* ip, int port)
{
	struct sockaddr_in addr;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketClosed(sock)) return INVALID_SOCKET;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	if (connect(sock, (struct sockaddr*)(&addr), sizeof(addr)) == 0)
	{
		SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
		SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);

		return sock;
	}

	SocketClose(sock);

	return INVALID_SOCKET;
}

SOCKET SocketConnectTimeout(const char* ip, int port, int timeout)
{
	u_long mode = 1;
	struct sockaddr_in addr;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketClosed(sock)) return INVALID_SOCKET;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	ioctlsocket(sock, FIONBIO, &mode); mode = 0;

	if (connect(sock, (struct sockaddr*)(&addr), sizeof(addr)) == 0)
	{
		SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
		SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);
		ioctlsocket(sock, FIONBIO, &mode);

		return sock;
	}

#ifdef XG_LINUX
	struct epoll_event ev;
	struct epoll_event evs;
	HANDLE handle = epoll_create(1);
	
	if (handle < 0)
	{
		SocketClose(sock);
	
		return INVALID_SOCKET;
	}
	
	memset(&ev, 0, sizeof(ev));
	
	ev.events = EPOLLOUT | EPOLLERR | EPOLLHUP;
	
	epoll_ctl(handle, EPOLL_CTL_ADD, sock, &ev);
	
	if (epoll_wait(handle, &evs, 1, timeout) > 0)
	{
		if (evs.events & EPOLLOUT)
		{
			int res = XG_ERROR;
			socklen_t len = sizeof(res);
	
			getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)(&res), &len);
			ioctlsocket(sock, FIONBIO, &mode);
	
			if (res == 0)
			{
				SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
				SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);
				Close(handle);
	
				return sock;
			}
		}
	}

	Close(handle);
#else
	struct timeval tv;

	fd_set ws;
	FD_ZERO(&ws);
	FD_SET(sock, &ws);

	tv.tv_sec = timeout / 1000;
	tv.tv_usec = timeout % 1000 * 1000;

	if (select(sock + 1, NULL, &ws, NULL, &tv) > 0)
	{
		int res = XG_ERROR;
		int len = sizeof(res);
	
		getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)(&res), &len);
		ioctlsocket(sock, FIONBIO, &mode);
	
		if (res == 0)
		{
			SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
			SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);

			return sock;
		}
	}
#endif

	SocketClose(sock);
	
	return INVALID_SOCKET;
}

BOOL GetSocketAddress(SOCKET sock, char* address)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);

	if (getpeername(sock, (struct sockaddr*)(&addr), &len) == 0)
	{
#ifdef XG_LINUX
		char buffer[32] = {0};
		const char* host = inet_ntop(AF_INET, &addr.sin_addr, buffer, sizeof(buffer));
#else
		const char* host = inet_ntoa(addr.sin_addr);
#endif
		if (host)
		{
			snprintf(address, 24, "%s:%d", host, (int)(ntohs(addr.sin_port)));

			return TRUE;
		}
	}

	strcpy(address, "UNKNOWN ADDRESS");

	return FALSE;
}

SOCKET ServerSocketAccept(SOCKET svr, char* address)
{
	SOCKET sock = INVALID_SOCKET;

	if (address)
	{
		struct sockaddr_in addr;
		socklen_t len = sizeof(addr);

		sock = accept(svr, (struct sockaddr*)(&addr), &len);

		if (IsSocketClosed(sock)) return INVALID_SOCKET;

#ifdef XG_LINUX
		char buffer[32] = {0};
		const char* host = inet_ntop(AF_INET, &addr.sin_addr, buffer, sizeof(buffer));
#else
		const char* host = inet_ntoa(addr.sin_addr);
#endif
		if (host)
		{
			snprintf(address, 24, "%s:%d", host, (int)(ntohs(addr.sin_port)));
		}
		else
		{
			strcpy(address, "UNKNOWN ADDRESS");
		}
	}
	else
	{
		sock = accept(svr, NULL, NULL);

		if (IsSocketClosed(sock)) return INVALID_SOCKET;
	}

	return sock;
}

const char* GetHostAddress(const char* host, char* ip)
{
#ifdef XG_LINUX
	struct addrinfo tmp;
	struct addrinfo* res;
	struct sockaddr_in* addr;

	memset(&tmp, 0, sizeof(tmp));

	tmp.ai_family = AF_INET;
	tmp.ai_flags = AI_PASSIVE;
	tmp.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(host, NULL, &tmp, &res) < 0) return NULL;

	addr = (struct sockaddr_in*)(res->ai_addr);
	ip = (char*)(inet_ntop(AF_INET, &addr->sin_addr, ip, 24));

	freeaddrinfo(res);
#else
	struct hostent* entry = gethostbyname(host);

	if (entry == NULL) return NULL;

	sprintf(ip, "%d.%d.%d.%d",
		(entry->h_addr_list[0][0] & 0x00ff),
		(entry->h_addr_list[0][1] & 0x00ff),
		(entry->h_addr_list[0][2] & 0x00ff),
		(entry->h_addr_list[0][3] & 0x00ff));
#endif
	return ip;
}

SOCKET CreateServerSocket(const char* ip, int port, int backlog)
{
	int val = 1;
	struct sockaddr_in addr;
	SOCKET sock = INVALID_SOCKET;

	sock = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketClosed(sock)) return INVALID_SOCKET;

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)(&val), sizeof(val));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	if (bind(sock, (const struct sockaddr*)(&addr), sizeof(addr)) == 0 && listen(sock, backlog) == 0) return sock;

	SocketClose(sock);

	return INVALID_SOCKET;
}

int SocketPeek(SOCKET sock, void* data, int size)
{
	int res = recv(sock, data, size, MSG_PEEK);

	if (res > 0) return res;

	if (res == 0) return XG_NETCLOSE;

	if (IsSocketTimeout()) return 0;

	return XG_NETERR;
}

int SocketRead(SOCKET sock, void* data, int size)
{
	return SocketReadEx(sock, data, size, TRUE);
}

int SocketReadEx(SOCKET sock, void* data, int size, BOOL completed)
{
	char* str = (char*)(data);

	if (completed)
	{
		int res = 0;
		int times = 0;
		int readed = 0;

		while (readed < size)
		{
			res = recv(sock, str + readed, size - readed, 0);

			if (res > 0)
			{
				if (res > SOCKET_TIMEOUT_LIMITSIZE)
				{
					times = 0;
				}
				else
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}

				readed += res;
			}
			else if (res == 0)
			{
				return XG_NETCLOSE;
			}
			else
			{
				if (IsSocketTimeout())
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}
				else
				{
					return XG_NETERR;
				}
			}
		}

		return readed;
	}
	else
	{
		int res = recv(sock, str, size, 0);

		if (res > 0) return res;

		if (res == 0) return XG_NETCLOSE;

		if (IsSocketTimeout()) return 0;

		return XG_NETERR;
	}
}

int SocketWrite(SOCKET sock, const void* data, int size)
{
	return SocketWriteEx(sock, data, size, TRUE);
}

int SocketWriteEx(SOCKET sock, const void* data, int size, BOOL completed)
{
	const char* str = (const char*)(data);

	if (completed)
	{
		int res = 0;
		int times = 0;
		int writed = 0;

		while (writed < size)
		{
			res = send(sock, str + writed, size - writed, 0);

			if (res > 0)
			{
				if (res > SOCKET_TIMEOUT_LIMITSIZE)
				{
					times = 0;
				}
				else
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}
	
				writed += res;
			}
			else
			{
				if (IsSocketTimeout())
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}
				else
				{
					return XG_NETERR;
				}
			}
		}
		
		return writed;
	}
	else
	{
		int res = send(sock, str, size, 0);

		if (res > 0) return res;

		if (IsSocketTimeout()) return 0;

		return XG_NETERR;
	}
}

int SocketReadLine(SOCKET sock, char* msg, int len)
{
	int val = 0;
	int readed = 0;

	while (readed < len)
	{
		if ((val = SocketReadEx(sock, msg + readed, len - readed, FALSE)) < 0) return val;

		if (val == 0 || (readed += val) < 2) continue;

		if (msg[readed - 2] == '\r' && msg[readed - 1] == '\n')
		{
			msg[readed - 2] = 0;

			return readed - 2;
		}
	}

	return XG_DATAERR;
}

int SocketWriteEmptyLine(SOCKET sock)
{
	return SocketWrite(sock, "\r\n", 2);
}
int SocketWriteLine(SOCKET sock, const char* msg, int len)
{
	if (len + 2 > 64 * 1024)
	{
		len = SocketWrite(sock, msg, len);

		return (len > 0) ? SocketWrite(sock, "\r\n", 2) : len;
	}
	else
	{
		char buffer[64 * 1024];

		memcpy(buffer, msg, len);
		buffer[len++] = '\r';
		buffer[len++] = '\n';

		return SocketWrite(sock, buffer, len);
	}
}

BOOL SocketSetSendTimeout(SOCKET sock, int ms)
{
#ifdef XG_LINUX
	struct timeval tv;

	tv.tv_sec = ms / 1000;
	tv.tv_usec = ms % 1000 * 1000;

	return setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char*)(&tv), sizeof(tv)) == 0;
#else
	return setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char*)(&ms), sizeof(ms)) == 0;
#endif
}

BOOL SocketSetRecvTimeout(SOCKET sock, int ms)
{
#ifdef XG_LINUX
	struct timeval tv;

	tv.tv_sec = ms / 1000;
	tv.tv_usec = ms % 1000 * 1000;

	return setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)(&tv), sizeof(tv)) == 0;
#else
	return setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)(&ms), sizeof(ms)) == 0;
#endif
}

#ifdef XG_LINUX

void* ThreadFunction(void* param)
{
	stWorkItem* item = (stWorkItem*)(param);

	item->func(item);
	free(item);

	pthread_detach(pthread_self());

	return NULL;
}

#else

DWORD WINAPI ThreadFunction(void* param)
{
	stWorkItem* item = (stWorkItem*)(param);

	item->func(item);
	free(item);

	return 0;
}
#endif

BOOL IsInvalidThread(THREAD_T thread)
{
	return thread == (THREAD_T)(-1) || thread == (THREAD_T)(NULL) ? TRUE : FALSE;
}

BOOL StartThread(stWorkItem item, const void* attr)
{
	THREAD_T thread;
	stWorkItem* param = (stWorkItem*)malloc(sizeof(stWorkItem));

	if (param == NULL) return FALSE;

	memcpy(param, &item, sizeof(stWorkItem));

#ifdef XG_LINUX
	if (pthread_create(&thread, (pthread_attr_t*)(attr), ThreadFunction, param))
	{
		free(param);

		return FALSE;
	}
#else
	thread = CreateThread((LPSECURITY_ATTRIBUTES)(attr), 0, ThreadFunction, param, 0, NULL);

	if (IsInvalidThread(thread))
	{
		free(param);

		return FALSE;
	}
#endif

	return TRUE;
}

/////////////////////////////////////////////////////////////////////

typedef struct __stWorkThread
{
	BOOL idle;
	stWorkItem item;
	MutexHandle lock;
	stThreadPool* pool;
} stWorkThread;


static void ThreadPoolRelease(stWorkThread* thread)
{
	LockMutex(thread->pool->lock);

	thread->idle = TRUE;
	thread->pool->cnt++;

	UnlockMutex(thread->pool->lock);
}

static void ThreadPoolProcess(stWorkItem* item)
{
	stWorkThread* thread = (stWorkThread*)(item->data);

	while (TRUE)
	{
		LockMutex(thread->lock);

		if (thread->item.func) thread->item.func(&thread->item);

		ThreadPoolRelease(thread);
	}
}

stThreadPool* ThreadPoolCreate(int maxsz)
{
	stWorkItem item;
	stThreadPool* pool = (stThreadPool*)malloc(sizeof(stThreadPool));
	stWorkThread* threads = (stWorkThread*)malloc(maxsz * sizeof(stWorkThread));

	if (pool == NULL || threads == NULL)
	{
		if (threads) free(threads);

		return NULL;
	}

	if (InitMutex(pool->lock))
	{
		item.func = ThreadPoolProcess;
		pool->threads = threads;
		pool->sz = maxsz;

		while (maxsz-- > 0)
		{
			threads->idle = TRUE;
			threads->pool = pool;
			item.data = threads;

			if (InitMutex(threads->lock) && LockMutex(threads->lock) && StartThread(item, NULL))
			{
				pool->cnt++;
				threads++;

				continue;
			}

			ErrorExit(XG_SYSERR);
		}

		return pool;
	}

	free(threads);
	free(pool);

	return NULL;
}

BOOL ThreadPoolDoWork(stThreadPool* pool, stWorkItem item)
{
	stWorkThread* str = (stWorkThread*)(pool->threads);
	stWorkThread* end = (stWorkThread*)(pool->threads) + pool->sz;

	CHECK_FALSE_RETURN(LockMutex(pool->lock));

	if (pool->cnt <= 0)
	{
		UnlockMutex(pool->lock);

		return FALSE;
	}

	while (str < end)
	{
		if (str->idle) break;

		++str;
	}

	str->idle = FALSE;
	pool->cnt--;

	UnlockMutex(pool->lock);

	memcpy(&str->item, &item, sizeof(item));

	UnlockMutex(str->lock);

	return TRUE;
}

void DllFileClose(DLLFILE_T handle)
{
#ifdef XG_LINUX
	dlclose(handle);
#else
	FreeLibrary((HMODULE)(handle));
#endif
}

DLLFILE_T DllFileOpen(const char* path)
{
#ifdef XG_LINUX
	return dlopen(path, RTLD_NOW);
#else
	return (DLLFILE_T)(LoadLibraryA(path));
#endif
}

void* DllFileGetAddress(DLLFILE_T handle, const char* name)
{
#ifdef XG_LINUX
	return (void*)(dlsym(handle, name));
#else
	return (void*)(GetProcAddress((HMODULE)(handle), name));
#endif
}
