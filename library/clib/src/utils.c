#include "../utils.h"
#include "../system.h"


#ifdef XG_LINUX

#include <pwd.h>
#include <utime.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/time.h>

int getch()
{
	struct termios tm;
	struct termios old;

	int ch;
	int fd = STDIN_FILENO;

	if (tcgetattr(fd, &tm) < 0) return XG_ERROR;

	old = tm;
	cfmakeraw(&tm);

	if (tcsetattr(fd, TCSANOW, &tm) < 0) return XG_ERROR;

	ch = fgetc(stdin);

	if (tcsetattr(fd, TCSANOW, &old) < 0) return XG_ERROR;

	return ch;
}

#else

BOOL usleep(unsigned long usec)
{
	HANDLE timer;
	LARGE_INTEGER delay;

	delay.QuadPart = 10 * usec;
	
	CHECK_FALSE_RETURN(HandleCanUse(timer = CreateWaitableTimer(NULL, TRUE, NULL)));
	CHECK_FALSE_RETURN(SetWaitableTimer(timer, &delay, 0, NULL, NULL, 0));
	WaitForSingleObject(timer, INFINITE);
	CloseHandle(timer);

	return TRUE;
}

#endif

BOOL DaemonInit()
{
	fflush(stdout);
	
#ifdef XG_LINUX
	pid_t pid = fork();
	
	if (pid < 0) return FALSE;
	if (pid > 0) exit(0);

	setsid(); umask(0);
#endif

	return TRUE;
}

BOOL ClearConsole()
{
#ifdef XG_LINUX
	return system("clear") >= 0;
#else
	return system("cls") >= 0;
#endif
}

void SystemExit(int code)
{
#ifdef XG_LINUX
	kill(getpid(), SIGINT);
	sync();
	sleep(1);
	_exit(code);
#else
	fflush(stdout);
	TerminateProcess(GetCurrentProcess(), code);
#endif
}

unsigned long long GetTime()  
{
#ifdef XG_LINUX
	struct timeval tv;
	struct timezone tz;

	gettimeofday(&tv, &tz);

	return tv.tv_sec * 1000000 + tv.tv_usec;
#else
	FILETIME ft;

	GetSystemTimeAsFileTime(&ft);  

	return ((((unsigned long long)(ft.dwHighDateTime) << 32) | ft.dwLowDateTime) - 116444736000000000L) / 10;
#endif
}

const char* GetLocaleCharset()
{
#ifdef _MSC_VER
	static char charset[] = "gbk";
#else
	static char charset[32] = {0};
#endif

	if (*charset) return charset;

	int idx = 0;
	char lang[64] = {0};
	const char* str = getenv("LANG");

	if (str == NULL || *str == 0) return "utf-8";

	while (*str && idx < ARR_LEN(lang) - 1)
	{
		lang[idx++] = tolower(*str++);
	}

	if (strstr(lang, "gbk"))
	{
		strcpy(charset, "gbk");
	}
	else if (strstr(lang, "utf-8"))
	{
		strcpy(charset, "utf-8");
	}
	else if (strstr(lang, "gb2312"))
	{
		strcpy(charset, "gb2312");
	}
	else
	{
		const char* tmp = strrchr(lang, '.');

		strncpy(charset, (tmp ? tmp + 1 : lang), sizeof(charset) - 1);
	}

	return charset;
}

const char* GetCurrentUser()
{
	static char name[0xFF] = { 0 };

	if (*name) return name;
	
#ifdef XG_LINUX
	struct passwd* pwd;
	
	if ((pwd = getpwuid(getuid()))) strncpy(name, pwd->pw_name, sizeof(name) - 1);
#else
	DWORD sz = sizeof(name) - 1;

	if (GetUserNameA(name, &sz))
	{
		name[sz] = 0;
	}
	else
	{
		name[0] = 0;
	}
#endif

	if (*name == 0) ErrorExit(XG_SYSERR);
	
	return name;
}

void SystemPause(const char* msg)
{
	if (msg == NULL) msg = "\npress any key to continue\n";

	if (*msg) puts(msg);

	fflush(stdout);
	getch();
}

BOOL GetDateTime(stDateTime* dt, const time_t* tm)
{
	time_t now;
	struct tm tmp;

	if (tm == NULL)
	{
		time(&now);
		tm = &now;
	}

	localtime_r(tm, &tmp);

	dt->year = tmp.tm_year + 1900;
	dt->month = tmp.tm_mon + 1;
	dt->yday = tmp.tm_yday + 1;
	dt->wday = tmp.tm_wday;
	dt->mday = tmp.tm_mday;

	if (dt->wday == 0) dt->wday = 7;

	dt->hour = tmp.tm_hour;
	dt->min = tmp.tm_min;
	dt->sec = tmp.tm_sec;

	return TRUE;
}

int GetHostInteger(const char* host)
{
	const char* str = host;
	u_char arr[sizeof(int)] = {0};

	arr[0] = atoi(str++);
	str = strchr(str, '.');

	if (str == NULL) return 0;

	arr[1] = atoi(++str);
	str = strchr(str, '.');
	
	if (str == NULL) return 0;

	arr[2] = atoi(++str);
	str = strchr(str, '.');

	if (str == NULL) return 0;

	arr[3] = atoi(++str);
	str = strchr(str, '.');
	
	return *(int*)(arr);
}

long GetFileLength(const char* path)
{
#ifdef XG_LINUX
	struct stat info;

	if (stat(path, &info) < 0) return XG_NOTFOUND;

	if (S_ISDIR(info.st_mode)) return XG_PARAMERR;

	return info.st_size;
#else
	if (GetPathType(path) == eNONE) return XG_NOTFOUND;

	long sz = 0;
	long ofs = 0;
	FILE* fp = fopen(path, "rb");

	if (fp == NULL) return XG_PARAMERR;

	ofs = ftell(fp);
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	fseek(fp, ofs, SEEK_SET);
	fclose(fp);

	return sz;
#endif
}

char* TrimString(char* str, const char* space)
{
	char* ptr = NULL;

	ptr = (char*)SkipStartString(str, space);

	if (*ptr == 0)
	{
		*str = 0;

		return str;
	}

	str = ptr + strlen(ptr) - 1;

	while (str > ptr && strchr(space, *str)) *str-- = 0;

	return ptr;
}

int GetStringCount(const char* str, const char* src)
{
	int count = 0;

	if (*str == 0) return 0;

	if (src[1] == 0)
	{
		char ch = *src;

		while (*str)
		{
			if (*str++ == ch) ++count;
		}
	}
	else
	{
		const char* end = NULL;
		const char* start = NULL;
		const int LEN = (int)(strlen(src));

		start = str;

		while (TRUE)
		{
			end = strstr(start, src);

			if (end == NULL)
			{
				size_t len = strlen(start);

				if (len > 0 && '\r' == start[len - 1]) --len;

				break;
			}
			else
			{
				start = end + LEN;
				++count;
			}
		}
	}

	return count;
}

stCharBuffer GetTrimString(const char* str, const char* space)
{
	size_t len = 0;
	stCharBuffer buffer;
	const char* ptr = NULL;

	buffer.val[0] = 0;

	ptr = SkipStartString(str, space);

	if (*ptr == 0) return buffer;

	len = strlen(ptr) - 1;

	while (len > 0 && strchr(space, ptr[len])) --len;

	if (len < 0) return buffer;

	memcpy(buffer.val, ptr, len + 1);
	buffer.val[len + 1] = 0;

	return buffer;
}

const char* SkipStartString(const char* str, const char* space)
{
	if (str == NULL || *str == 0) return str;

	while (*str && strchr(space, *str)) str++;

	return str;
}

char* GetLineValue(char* line, const char* tag, const char* spliter)
{
	char* ptr = NULL;
	size_t len = strlen(tag);

	line = (char*)SkipStartString(line, " \r\t");

	if (*line == 0 || strlen(line) <= len) return NULL;

	if (spliter == NULL) spliter = "=";

	if (memcmp(line, tag, len)) return NULL;

	line += len;
	len = strlen(spliter);

	line = (char*)SkipStartString(line, " \r\t");

	if (*line == 0 || strlen(line) <= len) return NULL;

	if (memcmp(line, spliter, len)) return NULL;

	ptr = line = (char*)SkipStartString(line + len, " \r\t");

	while (*ptr)
	{
		if (*ptr == ' ' || *ptr == '\r' || *ptr == '\t' || *ptr == '\n')
		{
			*ptr = 0;

			break;
		}

		++ptr;
	}

	return line;
}

stCharBuffer GetTruncatedString(const char* str, int len, const char* space)
{
	stCharBuffer dest;

	memcpy(dest.val, str, len);
	dest.val[len] = 0;
	
	return space ? GetTrimString(dest.val, space) : dest;
}

BOOL IsSmallEndianSystem()
{
	static u_int32 flag = 0;

	if (flag) return flag ? TRUE : FALSE;

	char data[] = {1, 0, 0, 0};
	u_int32* ptr = (u_int32*)(data);

	return (flag = *ptr) ? TRUE : FALSE;
}

BOOL IsAlphaString(const char* str)
{
	CHECK_FALSE_RETURN(isalpha(*str));

	++str;

	while (*str)
	{
		CHECK_FALSE_RETURN(isalpha(*str));

		++str;
	}

	return *str == 0;
}

BOOL IsAlnumString(const char* str)
{
	CHECK_FALSE_RETURN(isalnum(*str));

	++str;

	while (*str)
	{
		CHECK_FALSE_RETURN(isalnum(*str));

		++str;
	}

	return *str == 0;
}

BOOL IsNumberString(const char* str)
{
	const char* end = str + 16;

	CHECK_FALSE_RETURN((*str >= '0' && *str <= '9') || *str == '+' || *str == '-');

	if ((*str == '+' || *str == '-') && str[1] == 0) return FALSE;

	++str;

	while (*str && str < end)
	{
		CHECK_FALSE_RETURN(*str >= '0' && *str <= '9');

		++str;
	}

	return *str == 0;
}

BOOL IsAmountString(const char* str)
{
	const char* end = str + 16;

	CHECK_FALSE_RETURN((*str >= '0' && *str <= '9') || *str == '.' || *str == '+' || *str == '-');
	CHECK_FALSE_RETURN(GetStringCount(str, ".") < 2);

	++str;

	while (*str && str < end)
	{
		CHECK_FALSE_RETURN((*str >= '0' && *str <= '9') || *str == '.');

		++str;
	}

	return *str == 0;
}

void ReadPassword(char* str, int len, char echo, CHECK_PASSWORD_FUNC func)
{
	int i = 0;
	int ch = 0;
	BOOL flag = FALSE;
	
	if (isprint(echo)) flag = TRUE;

	while (i < len)
	{
		if ((ch = getch()) == '\b')
		{
			if (i == 0) continue;

			--i;

			if (flag) printf("\b \b");
		}
		else if (ch == 0 || ch == '\n' || ch == '\r')
		{
			break;
		}

		if (func == NULL)
		{
			if (isprint(ch))
			{
				if (flag) printf("%c", echo);

				str[i++] = (char)(ch);
			}
		}
		else if (func(ch))
		{
			if (flag) printf("%c", echo);

			str[i++] = (char)(ch);
		}
	}

	str[i] = 0;
}

#ifdef XG_LINUX

BOOL SetConsoleTextColor(E_CONSOLE_COLOR color)
{
#ifndef XG_AIX
	printf("\e[%dm", (int)(color));
#endif

	return TRUE;
}

time_t GetFileUpdateTime(const char* path)
{
	struct stat info;

	return stat(path, &info) == 0 ? info.st_mtime : XG_FAIL;
}

BOOL SetFileUpdateTime(const char* path, const time_t* tm)
{
	time_t now;
	struct utimbuf tmp;

	if (tm == NULL)
	{
		time(&now);
		tm = &now;
	}

	tmp.modtime = *tm;
	tmp.actime = *tm;

	return utime(path, &tmp) == 0;
}

int RunCommand(const char* cmd, char* buffer, int sz, BOOL hidewnd)
{
	FILE* fp = popen(cmd, "r");

	if (fp == NULL) return XG_SYSERR;

	if (buffer) sz = fread(buffer, 1, sz - 1, fp);

	pclose(fp);

	if (sz < 0) return XG_SYSERR;

	if (buffer) buffer[sz] = 0;

	return sz;
}

#else

BOOL SetConsoleTextColor(E_CONSOLE_COLOR color)
{
	static HANDLE console = NULL;

	if (console == NULL) console = GetStdHandle(STD_OUTPUT_HANDLE);

	CHECK_FALSE_RETURN(SetConsoleTextAttribute(console, color));

	return TRUE;
}

time_t GetFileUpdateTime(const char* path)
{
	HANDLE handle = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	if (handle)
	{
		struct tm t;
		FILETIME ft;
		SYSTEMTIME st;
		SYSTEMTIME utc;

		if (GetFileTime(handle, NULL, NULL, &ft) && FileTimeToSystemTime(&ft, &utc))
		{
			SystemTimeToTzSpecificLocalTime(NULL, &utc, &st);

			t.tm_year = st.wYear - 1900;
			t.tm_mon = st.wMonth - 1;
			t.tm_mday = st.wDay;

			t.tm_sec = st.wSecond;
			t.tm_min = st.wMinute;
			t.tm_hour = st.wHour;

			CloseHandle(handle);

			return mktime(&t);
		}

		CloseHandle(handle);
	}

	return XG_FAIL;
}

BOOL SetFileUpdateTime(const char* path, const time_t* tm)
{
	BOOL res;
	HANDLE file;
	FILETIME ft;
	stDateTime dt;
	SYSTEMTIME st;
	SYSTEMTIME utc;

	GetDateTime(&dt, tm);

	st.wDayOfWeek = dt.wday;
	st.wMilliseconds = 500;
	st.wMonth = dt.month;
	st.wMinute = dt.min;
	st.wSecond = dt.sec;
	st.wYear = dt.year;
	st.wHour = dt.hour;
	st.wDay = dt.day;

	file = Open(path, eWRITE);

	CHECK_FALSE_RETURN(HandleCanUse(file));

	TzSpecificLocalTimeToSystemTime(NULL, &st, &utc);
	SystemTimeToFileTime(&utc, &ft);

	res = SetFileTime(file, NULL, &ft, &ft);

	Close(file);

	return res;
}

int RunCommand(const char* cmd, char* buffer, int sz, BOOL hidewnd)
{
	char* args;
	HANDLE hRead = NULL;
	HANDLE hWrite = NULL;
	SECURITY_ATTRIBUTES sa;
	size_t len = strlen(cmd) + 0xFF;

	if (len < MAX_PATH) len = MAX_PATH;

	if ((args = (char*)malloc(len + len)) == NULL) return XG_SYSERR;

	snprintf(args, len, "cmd /c %s", cmd);

	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = NULL;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);

	if (CreatePipe(&hRead, &hWrite, &sa, XG_MEMFILE_MAXSZ))
	{
		STARTUPINFOA si;
		PROCESS_INFORMATION pi;

		memset(&si, 0, sizeof(si));
		memset(&pi, 0, sizeof(pi));

		si.cb = sizeof(si);
		si.hStdError = hWrite;
		si.hStdOutput = hWrite;
		si.wShowWindow = hidewnd ? SW_HIDE : SW_SHOW;
		si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;

		if (CreateProcessA(NULL, args, NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi))
		{
			DWORD readed = 0;

			WaitForSingleObject(pi.hProcess, INFINITE);
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
			CloseHandle(hWrite);
			free(args);

			if (buffer == NULL)
			{
				CloseHandle(hRead);

				return 0;
			}

			sz = ReadFile(hRead, buffer, (DWORD)(sz - 1), &readed, NULL) ? readed : -1;

			if (sz < 0 && GetLastError() == ERROR_BROKEN_PIPE) sz = 0;

			CloseHandle(hRead);

			if (sz >= 0)
			{
				buffer[sz] = 0;

				return sz;
			}
		}
		else
		{
			CloseHandle(hWrite);
			CloseHandle(hRead);
			free(args);
		}
	}

	return XG_SYSERR;
}

#endif
