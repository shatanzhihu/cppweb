#ifndef XG_HEADER_LIST
#define XG_HEADER_LIST
///////////////////////////////////////////////////////////////////////
#include "typedef.h"


#define stack_create list_create
#define queue_create list_create
#define queue_get list_pop_front
#define stack_get list_pop_front
#define queue_put list_push_back
#define stack_put list_push_front

#define list_size(_LIST_)	_LIST_->sz
#define queue_size(_LIST_)  _LIST_->sz
#define stack_size(_LIST_)  _LIST_->sz

typedef struct __stListNode
{
	void* data;
	struct __stListNode* next;
} stListNode;

typedef struct __stListHandle
{
	int sz;
	stListNode* head;
	stListNode* tail;
} stListHandle, stQueueHandle, stStackHandle;


typedef int(*ListQueryFunc)(stListNode*, void*);


#ifdef __cplusplus
extern "C" {
#endif
	stListHandle* list_create();

	void list_clear(stListHandle* handle);

	void list_destroy(stListHandle* handle);

	stListNode* list_create_node(void* data);

	void* list_pop_front(stListHandle* handle);

	void* list_remove(stListHandle* handle, void* data);

	void* list_push_back(stListHandle* handle, void* data);

	void* list_push_front(stListHandle* handle, void* data);

	void* list_query(stListHandle* handle, ListQueryFunc func, void* data);
#ifdef __cplusplus
}
#endif

///////////////////////////////////////////////////////////////////////
#endif
