<%@ path=${filename}%>
<%
	param_string(icon);
	param_string(code);
	param_string(name);
	param_int(hideside);
	param_string(title);
	param_string(folder);
	param_string(session);

	if (icon.empty()) icon = "/favicon.ico";

	if (name.empty())
	{
		if (hideside > 0) name = folder;

		if (name.empty()) name = webx::GetParameter("TITLE");
	}

	if (session.length() > 8 && session.length() < 1024)
	{
		char buffer[4096] = {0};

		HEXDecode(session.c_str(), session.length(), buffer);

		session = buffer;
	}

	try
	{
		checkLogin();
	}
	catch(Exception e)
	{
	}

	string client = stdx::tolower(request->getHeadValue("User-Agent"));

	if (client.find("android") == string::npos && client.find("iphone") == string::npos && client.find("ipad") == string::npos)
	{
		client = "PC";
	}
	else
	{
		client = "MOBILE";
	}
%>
<!DOCTYPE HTML>
<html manifest='/res/etc/manifest.cache'>
<head>
<title><%=name%></title>
<meta name='referrer' content='always'/>
<link rel='shortcut icon' href='<%=icon%>'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'/>
<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>

<link rel='stylesheet' type='text/css' href='/app/workspace/css/base.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/highlight/css/tomorrow.css'/>

<script src='/res/lib/utils.js.gzip'></script>
<script src='/res/lib/laydate/laydate.js.gzip'></script>
<script src='/res/lib/highlight/highlight.js.gzip'></script>

<script>
var curmenu = null;
var saveneeded = null;
var cursubmenu = null;
var curmenuitem = null;
var submenuicon = null;
var submenulist = null;
var automodifymap = {};
var modifyNotepad = null;
var cursubmenutitle = null;

function getMainView(){
	return $('#MainDiv');
}
function getFloatView(){
	return $('#FloatViewDiv');
}
function getSideWidth(){
	return $('#MainDiv').position().left;
}
function updateTitle(){
	$('#TitleDiv').html(getHttpResult('/TitlePage'));
}
function updateFooter(){
	$('#BottomDiv').load('/ShareNote?flag=S&title=FOOTER');
}
function setSaveNeeded(needed){
	saveneeded = needed;
}
function restoreContentSize(){
	$('#SubMenuDiv').width('100%');
}
function autoModifyContentSize(id){
	automodifymap[id] = id;
}
function showFloatQRCodeView(text, title, size){
	if (size == null) size = 120;

	var cx = size + 13;
	var cy = size + 30;
	var msg = "<div id='XG_FLOAT_CLOSE_BUTTON'></div><div id='XG_FLOAT_QRCODE_DIV'><table><tr><td id='XG_FLOAT_QRCODE_TD'><v-qrcode id='XG_FLOAT_QRCODE_IMAGE'></v-qrcode></td></tr>";

	if (text == null) text = window.location.href;

	if (title == null) title = '手机用户扫码访问';

	if (title.length > 0){
		msg += "<tr><td style='color:#667788;text-align:center;padding-top:2px'>" + title + "</td></tr>";
		cy += 16;
	}

	msg += "</table></div>";

	getFloatView().css('background', '#FFFFFF').css('border', '1px solid #CCDDEE').css('box-shadow', '2px 4px 6px #554433').width(cx).height(cy).html(msg).show();

	getVue('XG_FLOAT_QRCODE_DIV');

	$('#XG_FLOAT_CLOSE_BUTTON').click(function(){
		getFloatView().hide();
	});
	
	$('#XG_FLOAT_QRCODE_TD').width(size);

	$('#XG_FLOAT_QRCODE_IMAGE').val(text).attr('background', '#FFFFFF').attr('foreground', '#000000').attr('size', size).click();
	
	return getFloatView();
}
</script>

<%if (client == "PC"){%>

<link rel='stylesheet' type='text/css' href='/app/workspace/css/home.css'/>
<link rel='stylesheet' type='text/css' href='/app/workspace/css/menupad.css'/>
<link rel='stylesheet' type='text/css' href='/app/workspace/css/notepad.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/codemirror/lib/codemirror.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/codemirror/addon/fold/foldgutter.css'/>

<script src='/res/lib/kindeditor/kindeditor.js.gzip'></script>
<script src='/res/lib/kindeditor/lang/zh-cn.js.gzip'></script>
<script src='/res/lib/codemirror/lib/codemirror.js.gzip'></script>

<script src='/res/lib/codemirror/mode/clike/clike.js'></script>
<script src='/res/lib/codemirror/mode/shell/shell.js'></script>
<script src='/res/lib/codemirror/mode/markdown/markdown.js'></script>

<script src='/res/lib/codemirror/addon/fold/foldcode.js'></script>
<script src='/res/lib/codemirror/addon/fold/foldgutter.js'></script>
<script src='/res/lib/codemirror/addon/fold/brace-fold.js'></script>
<script src='/res/lib/codemirror/addon/fold/indent-fold.js'></script>
<script src='/res/lib/codemirror/addon/fold/comment-fold.js'></script>

<script>
function updateMenu(){
	curmenu = null;
	saveneeded = null;
	cursubmenu = null;
	curmenuitem = null;
	submenuicon = null;
	submenulist = null;

	$('#MenuTable').html('');
	$('#ContentDiv').html('');
	$('#SubMenuTable').html('');
	$('#BaiduTableDiv').css('display', 'none');

	getHttpResult('/GetUserMenuList', null, function(data){
		if (data.code < 0){
			showToast('加载数据失败');
		}
		else{
			$.each(data.list, function(idx, item){
				addMenu(item.folder, item.icon);
			});
			
			var title = '<%=title%>';
			var folder = '<%=folder%>';

			if (strlen(folder) == 0){
				title = getStorage('title');
				folder = getStorage('folder');
			}

			gotoMenu(folder, title, 50);

			$('#ListButton').click().click();
		}
	});
}
function hasSubmenu(title){
	if (submenulist == null || submenulist == '|') return false;
	return submenulist.indexOf('|' + title + '|') >= 0;
}
function addMenu(folder, icon){
	$('#MenuTable').append("<tr class='MenuItem'><td class='IconButton' style='background-image:url(" + icon + ")'></td><td class='MenuButton' value='" + folder + "'>" + folder + "</td></tr>");
	$('.MenuItem').last().click(function(){
		selectMenu($(this));
	});
	removeSession('usersubmenu:' + folder);
}
function gotoMenu(folder, title, delay){
	var flag = 0;

	$('.MenuItem').each(function(){
		if (folder == $(this).children().eq(1).attr('value')){
			selectMenu($(this), true, title, delay);
			flag = 1;
		}
	});

	if (flag == 0) selectMenu($('.MenuItem').first(), true, title, delay);
}
function addSubMenu(text, url, icon){
	$('#SubMenuTable').append("<td class='SubMenuItem' id='SubMenu" + getSequence() + "' value='" + url + "'><span class='SubIconButton' style='background-image:url(" + icon + ")'></span><span class='SubMenuButton'>" + text + "</span></td>");
	
	submenulist += text + '|';
	
	return $('.SubMenuItem').last().click(function(){
		selectSubMenu($(this));
	});
}
function updateSubMenu(){
	if (curmenuitem){
		var folder = curmenuitem.children().eq(1).attr('value');
		selectMenu(curmenuitem, true, cursubmenutitle);
		removeSession('usersubmenu:' + folder);
	}
}
function selectMenu(menu, updated, title, delay){
	var folder = menu.children().eq(1).attr('value');

	if (updated || curmenu == null || curmenu != folder){
		function update(data){
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if(data.code < 0){
				showToast("加载菜单数据失败");
			}
			else{
				var cnt = 0;
				var submenu = null;

				cursubmenu = null;
				curmenu = folder;

				if (data.code > 0){
					$.each(data.list, function(idx, item){
						if (strlen(title) > 0 && title == item.title){
							submenu = addSubMenu(item.title, item.url, item.icon);
						}
						else{
							addSubMenu(item.title, item.url, item.icon);
						}
		
						++cnt;
					});
				}
				
				if (cnt > 0){
					if (submenu == null) submenu = $('.SubMenuItem').first();
	
					selectSubMenu(submenu, null, delay);
				}
				else
				{
					sessionTimeout();
				}
			}

			return data.code;
		};

		submenulist = '|';

		$('#SubMenuTable').html('');

		if (updated) removeSession('usersubmenu:' + folder);

		try{
			update(JSON.parse(getSession('usersubmenu:' + folder)));
		}
		catch(e){
			getHttpResult('/GetUserSubMenuList', {user: 'winfeng', folder: folder}, function(data){
				if (update(data) > 0) setSession('usersubmenu:' + folder, JSON.stringify(data));
			});
		}
	}

	$('.MenuItem').css('background', 'none');
	menu.css('background-color', 'rgba(0, 80, 0, 0.8)');
	curmenuitem = menu;
}
function selectSubMenu(submenu, updated, delay){
	var url = null;
	var icon = null;

	if (submenu){
		url = submenu.attr('value');
		icon = getBackgroundImage(submenu.children().get(0));
	}

	if (updated || submenu || cursubmenu == null){
		if (saveneeded){
			saveneeded(function(){
				cursubmenu = url;
				submenuicon = icon;
				clearSingletonInterval();

				getFloatView().hide();
				$('#ContentDiv').html('');

				if (strlen(url) > 0) $('#ContentDiv').html(getHttpResult(url));

				$('.SubMenuItem').removeClass('SelectSubMenu').addClass('UnelectSubMenu');
				submenu.addClass('SelectSubMenu');
				saveneeded = null;
			});
		}
		else{
			cursubmenu = url;
			submenuicon = icon;
			clearSingletonInterval();

			getFloatView().hide();

			if (strlen(url) > 0){
				var msg = getHttpResult(url);

				if (delay){
					setTimeout(function(){
						$('#ContentDiv').html(msg);
					}, delay);
				}
				else{
					$('#ContentDiv').html(msg);
				}
			}
			else{
				$('#ContentDiv').html('');
			}
			
			$('.SubMenuItem').removeClass('SelectSubMenu').addClass('UnelectSubMenu');
			submenu.addClass('SelectSubMenu');
		}
	}

	cursubmenutitle = submenu.children('.SubMenuButton').html();

	restoreContentSize();
	setStorage('folder', curmenu);
	setStorage('title', cursubmenutitle);
}

window.onload = function(){
	var code = '<%=code%>';
	var mail = '<%=session%>';

	marked.setOptions({
		highlight: function(code){
			return hljs.highlightAuto(code).value;
		}
	});

	var top = 32;
	var side = 100;
	var bottom = 34;
	var grouplist = '';
	
	if (mail.length > 0 && code.length > 0){
		getHttpResult('/CheckLogin', {mail: mail, code: code}, function(data){
			window.location.href = '/';
		});
	}
	else{
		getHttpResult('/CheckLogin', {flag: 'C'}, function(data){
			if (data.code > 0){
				grouplist = data.grouplist;
				setCurrentUser(data.user);
			}
		});
	}
	
	<%if (hideside > 0){%>
		$('#BaiduTableDiv').css('left', '3px');
		$('#TitleIconDiv').remove();
		$('#ListButton').remove();
		side = 0;
	<%}%>

	setFrameStyle('TopDiv', 'SideDiv', 'MainDiv', 'BottomDiv', top, side, bottom, 1);

	getVue('TopDiv');
	updateFooter();
	updateTitle();
	updateMenu();

	$('#ListButton').click(function(){
		if ($(this).hasClass('BigListButton')){
			$(this).attr('title', '显示菜单');

			$('.MenuItem').each(function(){
				$(this).attr('title', $(this).children().eq(1).attr('value'));
			});

			$('.MenuButton').hide();
			$(this).addClass('SmallListButton');
			$(this).removeClass('BigListButton');
			setFrameStyle('TopDiv', 'SideDiv', 'MainDiv', 'BottomDiv', top, 30, bottom, 1);
			
			setStorage('biglistbutton', "0");
		}
		else{
			$(this).attr('title', '隐藏菜单');
			
			$('.MenuButton').show();
			$('.MenuItem').removeAttr('title');
			$(this).addClass('BigListButton');
			$(this).removeClass('SmallListButton');
			setFrameStyle('TopDiv', 'SideDiv', 'MainDiv', 'BottomDiv', top, side, bottom, 1);
			
			setStorage('biglistbutton', "1");
		}
		
		if (modifyNotepad) modifyNotepad();

		if (window.onresize) window.onresize();
	});

	if (getStorage('biglistbutton') == '0'){
		$('#ListButton').addClass('BigListButton').click();
	}
	else{
		$('#ListButton').addClass('SmallListButton').click();
	}

	$('#GotoTopDiv').hide().click(function(){
		getCtrl('MainDiv').scrollTop = 0;
		$('#GotoTopDiv').fadeOut();
	});

	$('#MainDiv').scroll(function(){
		for (var id in automodifymap){
			$('#' +  id).width(this.scrollWidth);
		}

		if (this.scrollTop > this.clientHeight){
			$('#GotoTopDiv').show();
		}
		else{
			$('#GotoTopDiv').fadeOut();
		}
	});

	$(window).resize(function(){
		var sz = getCtrl('MainDiv').scrollWidth;

		for (var id in automodifymap) $('#' +  id).width(sz);

		if (modifyNotepad) modifyNotepad();
	});

	autoModifyContentSize('SubMenuDiv');

	function getQueryText(){
		return encodeURIComponent($('#BaiduText').val());
	}

	if (strlen(grouplist) <= 0 || grouplist == 'normal'){
		$('#BaiduTableDiv').css('display', 'none');
	}
	else{
		$('#BaiduButton').click(function(){
			var text = getQueryText();
			if (text.length > 0){
				window.open('https://www.baidu.com/s?wd=' + text);
				$('#BaiduText').val('');
			}
			else{
				window.open('https://www.baidu.com');
			}
		});
		
		$('#YoudaoButton').click(function(){
			var text = getQueryText();
			if (text.length > 0){
				window.open('http://www.youdao.com/w/' + text);
				$('#BaiduText').val('');
			}
			else{
				window.open('http://www.youdao.com');
			}
		});

		$('#TaobaoButton').click(function(){
			var text = getQueryText();
			if (text.length > 0){
				window.open('https://s.taobao.com/search?q=' + text);
				$('#BaiduText').val('');
			}
			else{
				window.open('https://s.taobao.com');
			}
		});
		
		$('#GaodeButton').click(function(){
			var text = getQueryText();
			if (text.length > 0){
				window.open('http://ditu.amap.com/search?query=' + text);
				$('#BaiduText').val('');
			}
			else{
				window.open('http://ditu.amap.com');
			}
		});
		
		$('#StockButton').click(function(){
			var text = $('#BaiduText').val();

			if (text.length > 0){
				if (new RegExp(/^[0,3,6]\d{5}$/).test(text)){
					window.open("http://finance.sina.com.cn/realstock/company/" + (text < '600000' ? 'sz' : 'sh') + text + "/nc.shtml");
					$('#BaiduText').val('');
				}
				else{
					showToast('请输入正确的股票代码');
					$('#BaiduText').focus();
				}
			}
			else{
				window.open('http://finance.sina.com.cn/realstock');
			}
		});

		var curtext = null;
		var textpick = null;

		$('#BaiduText').keyup(function(e){
			if (e.keyCode == 13){
				$('#BaiduButton').click();
			}
			else if (e.keyCode < 37 || e.keyCode > 40){
				var self = this;
				var text = $(this).val();

				if (text.length > 0 && text != curtext){
					getHttpResult('/GetWords', {key: text}, function(data){
						if (data.code > 0){
							if (textpick) textpick.destroy();

							textpick = new AutoTextPicker(self, data.list);
							
							textpick.onclick = function(text){
								$('#BaiduButton').click();
							};
							
							$(self).blur(function(){
								textpick.hide();
							});
							
							curtext = text;
						}
					}, true);
				}
			}
		});
		
		$('#BaiduTableDiv').css('display', 'inline-block');
	}
}
</script>
</head>

<body>
	<div id='TopDiv'>
		<div id='TitleIconDiv'></div>
		<div id='ListButton'></div>
		<div id='StatusLabel'></div>
		<div id='TitleDiv'></div>
	</div>
	<div id='SideDiv'>
		<div>
			<table id='MenuTable' cellspacing='0px' cellpadding='0px'></table>
		</div>
	</div>
	<div id='MainDiv'>
		<div id='SubMenuDiv'>
			<table cellspacing='0px' cellpadding='0px'><tr id='SubMenuTable'></tr></table>
		</div>
		<div id='ContentDiv'></div>
	</div>
	<div id='BottomDiv'></div>
	<div id='GotoTopDiv'></div>
	<div id='FloatViewDiv'></div>
	<div id='BaiduTableDiv'>
		<table id='BaiduTable'>
			<tr>
				<td>
					<input class='TextField' id='BaiduText' autocomplete='off' type='text' size='40' maxlength='128' placeholder='输入你要搜索的内容'></input>
					<span class='TextButton' id='BaiduButton'>百度一下</span>
					<span class='TextButton' id='GaodeButton'>高德地图</span>
					<span class='TextButton' id='StockButton'>股市行情</span>
					<span class='TextButton' id='TaobaoButton'>淘宝搜索</span>
					<span class='TextButton' id='YoudaoButton'>有道词典</span>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>

<%}else{%>

<style>
.MenuItem{
	text-align: center;
}
.MenuIcon{
	width: 36px;
	height: 24px;
	display: inline-block;
	background-size: 60% 88%;
	background-position: center;
	background-repeat: no-repeat;
}
.MenuTitle{
	font-size: 0.7rem;
}
.SubMenuItem{
	margin: 8px;
	padding: 4px;
	text-align: center;
}
.SubMenuIcon{
	width: 60px;
	height: 40px;
	display: inline-block;
	background-size: 56% 80%;
	background-position: center;
	background-repeat: no-repeat;
}
.SubMenuTitle{
	font-size: 0.8rem;
}
#SinglePageDiv{
	padding-bottom: 32px;
}
#SinglePageMenuDiv{
	left: 0px;
	width: 100%;
	bottom: 0px;
	padding: 2px;
	position: fixed;
	background: #FFFFFF;
	border-top: 1px solid #DDEEFF;
}
</style>

<script>
function updateMenu(){
	curmenu = null;
	cursubmenu = null;
	curmenuitem = null;
	submenuicon = null;

	$('#MenuList').html('');

	getHttpResult('/GetUserMenuList', null, function(data){
		if (data.code < 0){
			showToast('加载数据失败');
		}
		else{
			var menu = null;
			var folder = '<%=folder%>';

			if (strlen(folder) == 0) folder = getStorage('folder');
			
			$.each(data.list, function(idx, item){
				var tmp = addMenu(item.folder, item.icon);
				if (folder == item.folder) menu = tmp;
			});

			selectMenu(menu || $('.MenuItem').first());

			$('.MenuItem').click(function(){
				selectMenu($(this));
			});
		}
	});
}
function addMenu(folder, icon){
	$('#MenuList').append("<td class='MenuItem' title='" + folder + "'><span class='MenuIcon' style='background-image:url(" + icon + ")'></span><div></div><span class='MenuTitle'>" + folder + "</span></td>");
	removeSession('usersubmenu:' + folder);
	return $('.MenuItem').last();
}
function addSubMenu(idx, text, url, icon){
	if (idx % 4 == 0) $('#SubMenuTable').append('<tr></tr>');
	if (text){
		$('#SubMenuTable').last('tr').append("<td class='SubMenuItem' title='" + text + "' link='" + url + "'><span class='SubMenuIcon' style='background-image:url(" + icon + ")'></span><div></div><span class='SubMenuTitle'>" + text + "</span></td>");
	}
	else{
		$('#SubMenuTable').last('tr').append("<td class='SubMenuItem'><span class='SubMenuIcon'></span><div></div><span class='SubMenuTitle'></span></td>");
	}
}
function selectMenu(menu, updated){
	var folder = menu.attr('title');

	if (updated || curmenu == null || curmenu != folder){
		function update(data){
			if (data.code == XG_TIMEOUT){
				sessionTimeout();
			}
			else if(data.code < 0){
				showToast("加载菜单数据失败");
			}
			else{
				cursubmenu = null;
				curmenu = folder;

				setStorage('folder', folder);

				if (data.code == 0){
					sessionTimeout();
				}
				else if (data.code == 1){
					getHttpResult(data.list[0].url, {}, function(data){
						$('#SinglePageDiv').html("<div style='text-align:center'><span style='text-align:left'>" + data + "</span></div>");
					}, true);
				}
				else{
					if (data.code > 0){
						var num = 0;

						$.each(data.list, function(idx, item){
							addSubMenu(num++, item.title, item.url, item.icon);
						});
						
						while (num % 4) addSubMenu(num++);
					}

					$('.SubMenuItem').click(function(){
						selectSubMenu($(this));
					});
				}
			}

			return data.code;
		};

		$('#SinglePageDiv').html("<table width='100%' id='SubMenuTable'></table>");

		if (updated) removeSession('usersubmenu:' + folder);

		try{
			update(JSON.parse(getSession('usersubmenu:' + folder)));
		}
		catch(e){
			getHttpResult('/GetUserSubMenuList', {folder: folder}, function(data){
				if (update(data) > 0) setSession('usersubmenu:' + folder, JSON.stringify(data));
			});
		}
	}

	curmenuitem = menu;
	$('.MenuItem').css('background', 'none');
	menu.css('background', '#789');
}
function selectSubMenu(submenu, updated){
	var path = null;
	var icon = null;
	var title = null;

	if (submenu){
		path = submenu.attr('link');
		title = submenu.attr('title');
		icon = getBackgroundImage(submenu.children().get(0));
	}

	if (updated || submenu || cursubmenu == null){
		cursubmenu = path;
		submenuicon = icon;
		clearSingletonInterval();

		if (strlen(path) > 0){
			window.location.href = '/SinglePage?align=center&path=' + encodeURIComponent(path) + '&icon=' + encodeURIComponent(icon) + '&title=' + encodeURIComponent(title);
		}
	}
}

window.onload = function(){
	var code = '<%=code%>';
	var mail = '<%=session%>';

	if (mail.length > 0 && code.length > 0){
		getHttpResult('/CheckLogin', {mail: mail, code: code}, function(data){
			window.location.href = '/';
		});
	}

	updateMenu();
	
	<%if (hideside > 0){%>
		$('#SinglePageMenuDiv').hide();
	<%}%>

	$('#GotoTopDiv').hide().click(function(){
		getCtrl('SinglePageDiv').scrollTop = 0;
		$('#GotoTopDiv').fadeOut();
	});
}
</script>
</head>

<body>
	<div id='GotoTopDiv'></div>
	<div id='FloatViewDiv'></div>
	<div id='SinglePageDiv'></div>
	<div id='SinglePageMenuDiv'>
		<table width='100%'><tr id='MenuList'></tr></table>
	</div>
</body>
</html>

<%}%>