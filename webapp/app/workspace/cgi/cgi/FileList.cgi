<%@ path=${filename}%>
<%
	param_string(root);
	
	if (root.empty()) root = "/dat/pub";
%>
<style>
#FilePathTable{
	margin: 3px 0px;
}
#FilePathTable td{
	padding: 1px;
}
#FileRecordTable{
	width: 100%;
}
#FileRecordTable td{
	padding: 1px 4px;
}
#FileRecordTable tr{
	height: 32px;
}
#FileRecordTable button{
	float: none;
	margin: 2px;
	padding: 1px 2px;
	font-size: 0.88rem;
}
#FileRecordTable tr:nth-child(odd){
	background: rgba(100, 160, 200, 0.4);
}
#FileRecordTable tr:nth-child(even){
	background: rgba(180, 180, 180, 0.4);
}
#FileRecordTable tr:first-child{
	background: rgba(80, 80, 80, 0.5);
}
#FileRecordTable tr:first-child td{
	font-size: 0.9rem;
	font-weight: bold;
}
#AddFileButton{
	margin-left: 4px;
}
#FilePathLabel{
	color: #465;
	padding: 3px 2px;
	font-weight: bold;
	border-radius: 1px;
	background: #CCC;
	box-shadow: 1px 1px 3px #345;
	margin-right: 1px;
}
#FilePathText{
	color: #283;
	margin-left: 64px;
}
.FileLink{
	color: #283;
	text-decoration: none;
}
.FolderLink{
	color: #321;
	text-decoration: none;
}
.PathLabel{
	border: none;
	cursor: default;
	padding: 3px 2px;
	margin-right: 1px;
	border-radius: 1px;
	background-color: #CCC;
	box-shadow: 1px 1px 3px #345;
}
.PathLabel:hover{
	background-color: #89A;
	box-shadow: 2px 2px 3px #123;
}
.PathLabel:active{
	background-color: #789;
}
.FileIconTd{
	width: 28px;
	text-align: center;
}
.FileIconTd img{
	width: 24px;
	height: 24px;
}
.FileNameTd{
	width: 400px;
	max-width: 800px;
	text-align: left;
}
.FileTimeTd{
	width: 160px;
	text-align: center;
}
.FileSizeTd{
	width: 80px;
	text-align: center;
}
.FileRemarkTd{
	min-width: 80px;
	text-align: left;
}
.FileOperTd{
	width: 80px;
	min-width: 80px;
	text-align: center;
}
</style>

<div id='FileRecordDiv'>
	<table id='FilePathTable'></table>
	<table id='FileRecordTable'></table>
</div>

<script>
var filelist = null;
var sysright = false;
var rootpath = '<%=root%>';

autoModifyContentSize('FileRecordTable');

function getCompletdPath(path){
	var parent = strlen(rootpath) > 0 ? rootpath + '/' : '/';

	if (parent == '//') parent = '/';

	return strlen(path) > 0 ? parent + path + '/' : parent;
}

function addFileRecord(icon, name, type, size, url, remark, datetime){
	var oper = '';

	if (icon == null){
		name = '文件名称';
		type = '文件类型';
		size = '文件大小';
		oper = "文件操作";
		remark = '文件说明';
		datetime = '更新时间';
		$('#FileRecordTable').html('');
	}
	else{
		var title = name;

		if (type == 0) size = '';

		oper += "<button class='FileOperButton' name='" + name + "' type='" + type + "' remark='" + remark + "'>修改</button>"
		oper += "<button class='FileOperButton' name='" + name + "' type='" + type + "'>删除</button>"

		if (type == 0){
			name = "<a class='FolderLink' href='javascript:void(0)'>" + name + "</a>";
		}
		else{
			name = "<a class='FileLink' download='" + name + "' href='" + url + "'>" + name + "</a>";
		}
	}
	
	var html = "<tr class='FileRecordTr'>";
	
	html += "<td class='FileIconTd'>" + (icon ? "<img src='" + icon + "'/>" : "图标") + "</td>";
	html += "<td class='FileNameTd'>" + name + "</td>";
	
	if (machinetype == 'PC'){
		if (remark.indexOf('http://') == 0 || remark.indexOf('https://') == 0){
			remark = "<a class='FileLink' target='_blank' href='" + remark + "'>" + remark + "</a>"
		}

		html += "<td class='FileSizeTd'>" + size + "</td>";
		html += "<td class='FileTimeTd'>" + datetime + "</td>";
		html += "<td class='FileRemarkTd'>" + remark + "</td>";
		
		if (sysright){
			html += "<td class='FileOperTd'>" + oper + "</v-button></td>";
		}
	}

	html += "</tr>";

	$('#FileRecordTable').width('100%').append(html);
}

function deleteFileItem(path, name, type){
	var title = type ? '删除文件' : '删除目录';
	
	showConfirmMessage('是否要' + title + '[' + name + ']？', title, function(flag){
		if (flag == 1){
			getHttpResult('/MoveFile', {src: getCompletdPath(path) + name, flag: 'D'}, function(data){
				if (data.code == XG_TIMEOUT){
					sessionTimeout();
				}
				else{
					loadFileRecord(path);
				}
			});
		}
	});
}

function updateFileItem(path, name, type, remark){
	var url = '';
	var html = '';
	var flag = 'U';
	var title = type ? '文件' : '目录';
	
	if (strlen(name) == 0){
		flag = type ? 'A' : 'C';
		title = '新建' + title;
	}
	else{
		title = '修改' + title;
	}
	
	if (type == 0){
		html = "<table class='DialogTable'><tr><td><v-text id='FileNameText' title='目录名称' placeholder='输入目录名称' size='20' maxlength='64'></v-text></td></tr><tr><td><v-textarea id='FileRemarkText' title='目录说明' maxlength='256' height='36'></v-textarea></td></tr></table>";
	}
	else{
		html = "<table class='DialogTable'><tr><td><v-text id='FileNameText' title='文件名称' placeholder='输入文件名称' maxlength='64'></v-text></td></tr><tr><td id='UploadFileDiv'></td></tr><tr><td><v-textarea id='FileRemarkText' title='文件说明' maxlength='256' height='36'></v-textarea></td></tr></table>";
	}

	showConfirmMessage(html, title, function(code){
		if (code){
			var text = $('#FileNameText').val();
			var remark = $('#FileRemarkText').val();

			if (strlen(text) == 0){
				showToast((type ? '文件' : '目录') + '名称' + '不能为空');
				$('#FileNameText').focus();
				return false;
			}

			var src = '';
			var dest = '';
			var temp = '';
			var found = false;
			
			$.each(filelist, function(idx, item){
				if (text == item.name) found = true;
			});

			if (found){
				if (flag == 'A' || flag == 'C' || text != name){
					showToast('当前目录下已有名称相同的文件或目录');
					$('#FileNameText').focus();
					return false;
				}
			}
			
			if (flag == 'C'){
				src = getCompletdPath(path) + text;
			}
			else{
				if (strlen(url) == 0){
					if (flag == 'A'){
						showToast('请先上传文件');
						return false;
					}
					
					url = getCompletdPath(path) + name;
				}
				else{
					temp = text == name ? '' : getCompletdPath(path) + name;
				}
				
				src = url;
				dest = getCompletdPath(path) + text;
			}

			getHttpResult('/MoveFile', {src: src, dest: dest, flag: flag, remark: remark}, function(data){
				if (data.code == XG_TIMEOUT){
					sessionTimeout();
				}
				else{
					if (strlen(temp) > 0 && flag == 'U'){
						getHttpResult('/MoveFile', {src: temp, flag: 'D'});
					}
					
					loadFileRecord(path);
				}
			});
		}
	});
	
	$('#FileNameText').val(name);
	$('#FileRemarkText').val(remark);

	if (type == 0){
		$('#FileRemarkText').width($('#FileNameText').width());
	}
	else {
		uploadbutton = new UploadFileWidget('UploadFileDiv', '上传文件', '16px', 64 * 1024 * 1024);
		uploadbutton.label.attr('placeholder', '点击选择文件');
		$('#FileNameText').width(uploadbutton.label.width());
		$('#FileRemarkText').width(uploadbutton.label.width());
		uploadbutton.callback(function(data){
			if (data == null || data.code < 0){
				showErrorToast('文件上传失败');
			}
			else{
				var text = $('#FileNameText').val();
				
				url = data.url[0];

				if (strlen(text) == 0){
					$('#FileNameText').val(uploadbutton.label.val());
				}
				else{
					if (url.lastIndexOf('.') > 0 && text.lastIndexOf('.') < 0){
						$('#FileNameText').val(text + url.substring(url.lastIndexOf('.')));
					}
				}
			}
		});
	}
}

function updatePathList(path, foldercnt, filecnt){
	var pathhtml = "<tr><td><span id='FilePathLabel'>文件路径</span></td><td></td><td><span class='PathLabel' path=''>根目录</span></td>"

	if (path.length > 0){
		var curpath = '';
		var pathlist = path.split('/');
		
		$.each(pathlist, function(idx, item){
			curpath = (curpath.length == 0 ? item : curpath + '/' + item);
			pathhtml += "<td><span class='PathLabel' path='" + curpath + "'>" + item + "</span></td>"
		});
	}

	if (sysright && machinetype == 'PC'){	
		pathhtml += "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td class='TextButton' id='AddFolderButton'>新建目录</td><td></td><td class='TextButton' id='AddFileButton'>上传文件</td>";
	}
	
	pathhtml += "</tr>";
	
	$('#FilePathTable').html(pathhtml);
	
	getVue('FileRecordDiv');
	
	$('.PathLabel').click(function(){
		loadFileRecord($(this).attr('path'));
	});

	$('#AddFileButton').click(function(){
		updateFileItem(path, '', 1);
	});
	
	$('#AddFolderButton').click(function(){
		updateFileItem(path, '', 0);
	});
	
	$('.FileOperButton').addClass('TextButton').click(function(){
		var name = $(this).attr('name');
		var type = $(this).attr('type');
		var remark = $(this).attr('remark');

		if ($(this).text() == '删除'){
			deleteFileItem(path, name, type);
		}
		else{
			updateFileItem(path, name, type, remark);
		}
	});
}

function loadFileRecord(path){
	getHttpResult('/GetFileList', {path: path, root: rootpath}, function(data){
		sysright = data.system;
		filelist = null;
		addFileRecord();

		if (data.code == XG_TIMEOUT){
			sessionTimeout();
		}
		else if (data.code < 0){
			showToast('加载数据失败');
		}
		else{
			var filecnt = 0;
			var foldercnt = 0;
			
			filelist = data.list;

			if (data.code > 0){
				data.list.sort(function(a, b){
					if (a.type > b.type) return 1;
					if (a.type < b.type) return -1;
					if (a.name < b.name) return -1;
					if (a.name > b.name) return 1;
					return 0;
				});

				$.each(data.list, function(idx, item){
					addFileRecord(item.icon, item.name, item.type, item.size, item.url, item.remark, item.datetime);
					
					(item.type == 1) ? filecnt++ : foldercnt++;
				});
			}

			updatePathList(path, foldercnt, filecnt);

			$('.FolderLink').click(function(){
				var name = $(this).text();
				
				if (path.length > 0){
					loadFileRecord(path + '/' + name);
				}
				else{
					loadFileRecord(name);
				}
			});
		}
	}, true);
}
  
loadFileRecord('');
</script>