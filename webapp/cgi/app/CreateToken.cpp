#include <webx/menu.h>
#include <webx/route.h>

class CreateToken : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(CreateToken, CGI_PROTECT)

int CreateToken::process()
{
	param_int(timeout);
	param_string(token);

	if (timeout < 1 || token.empty()) return simpleResponse(XG_PARAMERR);

	char address[64] = {0};

	GetSocketAddress(response->getSocket()->getHandle(), address);

	char* str = strchr(address, ':');

	if (str == NULL) return simpleResponse(XG_SYSERR);
	
	*str = 0;

	HostItem item = webx::GetRegCenterHost();

	if (item.host == address)
	{
		session = webx::GetLocaleSession("TOKEN[" + token + "]", timeout);
	}
	else
	{
		if (IsLocalHost(address)) session = webx::GetLocaleSession("TOKEN[" + token + "]", timeout);
	}

	return simpleResponse(session ? XG_OK : XG_AUTHFAIL);
}