#include <webx/menu.h>

class GetIconList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetIconList)

int GetIconList::process()
{
	param_string(path);

	int res = 0;
	vector<string> vec;
	string urlpath = "/res/img/";
	string filepath = app->getPath() + urlpath.substr(1);

	webx::CheckFilePath(path, 0);

	if (path.length() > 0)
	{
		filepath += path + "/";
		urlpath += path + "/";
		
		urlpath = stdx::replace(urlpath, "\\", "/");
		urlpath = stdx::replace(urlpath, "//", "/");
	}

	stdx::GetFolderContent(vec, filepath, eFILE);

	JsonElement arr = json.addArray("list");

	for (string& item : vec)
	{
		arr[res++] = urlpath + item;
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}