#include <webx/menu.h>

class GetFileList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetFileList)

#ifndef XG_LINUX

string get_utf_string(const string& str)
{
	int len;
	string res;

	len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);

	wchar_t* wstr = new wchar_t[len + 1];
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, wstr, len);
	len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);

	char* dest = new char[len + 1];
	WideCharToMultiByte(CP_UTF8, 0, wstr, -1, dest, len, NULL, NULL);
	dest[len] = 0;
	res = dest;

	delete[] wstr;
	delete[] dest;

	return res;
}

#endif

int GetFileList::process()
{
	param_string(path);
	param_string(root);

	if (root.empty()) root = "/dat/pub";

	int res = 0;
	string sqlcmd;
	vector<string> vec;
	map<string, string> memap;
	string urlpath = root + "/" + path;
	string filepath = app->getPath() + urlpath.substr(1);

	webx::CheckFilePath(root, 0);
	webx::CheckFilePath(path, 0);

	if (path.length() > 0)
	{
		filepath += "/";
		urlpath += "/";
	}

#ifndef XG_LINUX
	response->setContentType("text/plain;charset=gbk");
#endif

	urlpath = stdx::replace(urlpath, "\\", "/");
	urlpath = stdx::replace(urlpath, "//", "/");
	
	filepath = stdx::syscode(filepath);
	urlpath = stdx::syscode(urlpath);

	stdx::GetFolderContent(vec, filepath);

	for (auto& item : vec)
	{
#ifdef XG_LINUX
		sqlcmd += ",'" + urlpath + item + "'";
#else
		sqlcmd += ",'" + get_utf_string(urlpath + item) + "'";
#endif
	}

	if (sqlcmd.length() > 0)
	{
		sqlcmd = "SELECT ID,REMARK FROM T_XG_FILE WHERE ID IN (" + sqlcmd.substr(1) + ")";

		sp<RowData> row;
		sp<DBConnect> dbconn = webx::GetDBConnect();
		sp<QueryResult> result = dbconn->query(sqlcmd);
		
		if (!result) return simpleResponse(XG_SYSBUSY);

		while (row = result->next())
		{
			memap[stdx::syscode(row->getString(0))] = stdx::syscode(row->getString(1));
		}
	}

	JsonElement arr = json.addArray("list");

	for (auto& item : vec)
	{
		string url = urlpath + item;
		string filename = filepath + item;

		JsonElement data = arr[res++];
		int sz = XFile::GetFileSize(filename);
		time_t utime = GetFileUpdateTime(filename.c_str());

		if (sz < 0)
		{
			data["icon"] = "/res/img/menu/folder.png";
			data["type"] = (int)(0);
			data["size"] = (int)(0);
		}
		else
		{
			data["icon"] = webx::GetFileIcon(item);
			data["type"] = (int)(1);
			data["size"] = sz;
			
			if (sz < 256 * 1024)
			{
				string extname = stdx::tolower(stdx::GetExtNameFromPath(item));

				if (extname == "jpg" || extname == "png" || extname == "gif" || extname == "jpeg")
				{
					data["icon"] = url + "?utime=" + stdx::str(utime);
				}
			}
		}

		data["url"] = url;
		data["name"] = item;
		data["remark"] = memap[url];
		data["datetime"] = utime ? DateTime(utime).toString() : stdx::EmptyString();
	}

	try
	{
		checkLogin();
		checkSystemRight();

		json["system"] = (int)(1);
	}
	catch(Exception e)
	{
		json["system"] = (int)(0);
	}

	json["path"] = path;
	json["code"] = res;
	out << json;

	return XG_OK;
}