import imp;

def update(py):
	return imp.reload(py);
	
def getcgimap():
	cgimap = {};

	from pyc import testdemo, textlogo, checkcode;
	cgimap['testdemo'] = lambda app: update(testdemo).main(app);
	cgimap['textlogo'] = lambda app: update(textlogo).main(app);
	cgimap['checkcode'] = lambda app: update(checkcode).main(app);

	from pyc.user import editwebsite, websitelist, websiteicon;
	cgimap['user/websiteicon'] = lambda app: update(websiteicon).main(app);
	cgimap['user/websitelist'] = lambda app: update(websitelist).main(app);
	cgimap['user/editwebsite'] = lambda app: update(editwebsite).main(app);

	from pyc.toolkit import htmtopdf, notetopdf, getwebinfo;
	cgimap['toolkit/htmtopdf'] = lambda app: update(htmtopdf).main(app);
	cgimap['toolkit/notetopdf'] = lambda app: update(notetopdf).main(app);
	cgimap['toolkit/getwebinfo'] = lambda app: update(getwebinfo).main(app);

	return cgimap;
