#include <webx/menu.h>


class MainApplication : public webx::Application
{
public:
	int process()
	{
		//获取请求参数
		param_string(key);
		param_string(value);

		//初始化数据库配置
		webx::InitDatabase();

		sp<DBConnect> db = webx::GetDBConnect();

		LogTrace(eINF, "获取数据库连接成功");

		sp<QueryResult> rs = db->query("SELECT ?", DateTime::ToString());
		
		if (rs)
		{
			sp<RowData> row;

			while (row = rs->next())
			{
				json["datetime"] = row->getString(0);
			}
		}

		//返回数据
		out << json;

		return XG_OK;
	}
};

START_WEB_APP(MainApplication)