#include <stdx/all.h>


vector<string> pathlist = {
	"$SOURCE_HOME/webapp/dat"
};


class MainApplication : public Application
{
public:
	bool main()
	{
		vector<string> vec;

		LogThread::Instance()->init("log");

		LogTrace(eINF, "enter cleanup loop ...");

		for (auto& path : pathlist) path = stdx::translate(path);

		while (true)
		{
			try
			{
				time_t now = time(NULL);

				for (const string& path : pathlist)
				{
					stdx::GetFolderContent(vec, path, eFILE);

					for (const string& name : vec)
					{
						string filepath = path + "/" + name;
						time_t filetime = GetFileUpdateTime(filepath.c_str());

						if (now > filetime + 60 * 60)
						{
							LogTrace(eINF, "file[%s][%s] has expired", filepath.c_str(), DateTime(filetime).toString().c_str());

							RemoveFile(filepath.c_str());
						}
					}
				}
			}
			catch(Exception e)
			{
				LogTrace(eERR, "exception[%s]", e.getErrorString());
			}

			sleep(60);
		}

		return true;
	}
};

START_APP(MainApplication)
