#include "T_XG_MENU.h"


void CT_XG_MENU::clear()
{
	this->id.clear();
	this->folder.clear();
	this->title.clear();
	this->icon.clear();
	this->url.clear();
	this->enabled.clear();
	this->remark.clear();
	this->position.clear();
	this->statetime.clear();
}
int CT_XG_MENU::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_MENU(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->folder.toValueString(conn->getSystemName());
	vec.push_back(&this->folder);
	sql += ",";
	sql += this->title.toValueString(conn->getSystemName());
	vec.push_back(&this->title);
	sql += ",";
	sql += this->icon.toValueString(conn->getSystemName());
	vec.push_back(&this->icon);
	sql += ",";
	sql += this->url.toValueString(conn->getSystemName());
	vec.push_back(&this->url);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->position.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_MENU::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->folder = row->getString(1);
	this->folder.setNullFlag(row->isNull());
	this->title = row->getString(2);
	this->title.setNullFlag(row->isNull());
	this->icon = row->getString(3);
	this->icon.setNullFlag(row->isNull());
	this->url = row->getString(4);
	this->url.setNullFlag(row->isNull());
	this->enabled = row->getLong(5);
	this->enabled.setNullFlag(row->isNull());
	this->remark = row->getString(6);
	this->remark.setNullFlag(row->isNull());
	this->position = row->getLong(7);
	this->position.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(8);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_MENU::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_MENU";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_MENU::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_MENU::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_MENU::update(bool nullable)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_MENU SET ";
	if (nullable || !this->folder.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FOLDER=";
		sql += this->folder.toValueString(conn->getSystemName());
		v.push_back(&this->folder);
	}
	if (nullable || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (nullable || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (nullable || !this->url.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "URL=";
		sql += this->url.toValueString(conn->getSystemName());
		v.push_back(&this->url);
	}
	if (nullable || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->position.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "POSITION=";
		sql += this->position.toValueString(conn->getSystemName());
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_MENU::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_MENU";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_MENU::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_MENU::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "FOLDER") return this->folder.toString();
	if (key == "TITLE") return this->title.toString();
	if (key == "ICON") return this->icon.toString();
	if (key == "URL") return this->url.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "POSITION") return this->position.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_MENU::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "FOLDER")
	{
		this->folder = val;
		return true;
	}
	if (key == "TITLE")
	{
		this->title = val;
		return true;
	}
	if (key == "ICON")
	{
		this->icon = val;
		return true;
	}
	if (key == "URL")
	{
		this->url = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "POSITION")
	{
		this->position = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_MENU::update(const string& condition, bool nullable, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_MENU SET ";
	if (nullable || !this->folder.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FOLDER=";
		sql += this->folder.toValueString(conn->getSystemName());
		v.push_back(&this->folder);
	}
	if (nullable || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (nullable || !this->icon.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ICON=";
		sql += this->icon.toValueString(conn->getSystemName());
		v.push_back(&this->icon);
	}
	if (nullable || !this->url.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "URL=";
		sql += this->url.toValueString(conn->getSystemName());
		v.push_back(&this->url);
	}
	if (nullable || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->position.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "POSITION=";
		sql += this->position.toValueString(conn->getSystemName());
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
