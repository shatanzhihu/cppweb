#ifndef XG_WEBX_ROUTE_H
#define XG_WEBX_ROUTE_H
///////////////////////////////////////////////////////////
#include "std.h"

namespace webx
{
	int GetLastRemoteStatus();
	HostItem GetRegCenterHost();
	void CheckSystemRight(ProcessBase* proc);
	HostItem GetRouteHost(const string& path);
	int UpdateRouteList(const string& host, int port);
	int Broadcast(const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	SmartBuffer GetRemoteResult(const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	int NotifyHost(const string& host, int port, const string& path, const string& param = "", const string& contype = "", const string& cookie = "");

	template<class RESPONSE> RESPONSE GetRemoteResult(const string& path, const Object& param, const string& contype = "", const string& cookie = "")
	{
		RESPONSE res;
		SmartBuffer data = GetRemoteResult(path, param.toString(), contype, cookie);

		if (data.str() && res.fromString(data.str())) return std::move(res);

		int code = GetLastRemoteStatus();

		stdx::Throw(code < 0 ? code : XG_ERROR, "grasp remote request[" + path + "] failed");

		return res;
	}
};

///////////////////////////////////////////////////////////
#endif
