#ifndef XG_WEBX_MENU_CPP
#define XG_WEBX_MENU_CPP
///////////////////////////////////////////////////////////
#include "../menu.h"

int webx::GetMenuSet(set<MenuItem>& dataset)
{
	typedef int (*GetMenuSetFunc)(set<MenuItem>&);
	static GetMenuSetFunc func = (GetMenuSetFunc)Process::GetObject("HTTP_GET_MENU_SET_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(dataset);
}

int webx::GetGroupSet(set<GroupItem>& dataset)
{
	typedef int (*GetGroupSetFunc)(set<GroupItem>&);
	static GetGroupSetFunc func = (GetGroupSetFunc)Process::GetObject("HTTP_GET_GROUP_SET_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(dataset);
}

void webx::InitNewUser(const string& user)
{
	typedef void (*AsyncInitNewUserFunc)(const char*);
	static AsyncInitNewUserFunc func = (AsyncInitNewUserFunc)Process::GetObject("HTTP_ASYNC_INIT_NEW_USER");

	if (func) func(user.c_str());
}

string webx::GetParameter(const string& id)
{
	typedef int (*GetParameterFunc)(const char*, char*, int);
	static GetParameterFunc func = (GetParameterFunc)Process::GetObject("HTTP_GET_PARAMETER");

	if (func == NULL) return stdx::EmptyString();

	char buffer[8 * 1024];
	
	if (func(id.c_str(), buffer, sizeof(buffer)) > 0) return buffer;

	return stdx::EmptyString();
}

string webx::GetConfileContent(const string& title)
{
	typedef int (*GetConfileContentFunc)(const char*, char*, int);
	static GetConfileContentFunc func = (GetConfileContentFunc)Process::GetObject("HTTP_GET_CONFILE_CONTENT");

	if (func == NULL) return stdx::EmptyString();

	char buffer[64 * 1024];
	
	if (func(title.c_str(), buffer, sizeof(buffer)) > 0) return buffer;

	return stdx::EmptyString();
}

sp<DBConnect> webx::GetDBConnect(const string& dbid)
{
	static HttpServer* app = HttpServer::Instance();

	if (dbid.empty())
	{
		sp<DBConnect> conn = app->getDBConnect();
		
		if (!conn)
		{
			LogTrace(eERR, "connect database failed");

			stdx::Throw(XG_SYSBUSY, "connect database failed");
		}

		return conn;
	}
	else
	{
		typedef DBConnectPool* (*GetDBConnectPoolFunc)(const char*);
		static GetDBConnectPoolFunc func = (GetDBConnectPoolFunc)Process::GetObject("HTTP_GET_DBCONNECT_POOL");

		if (func == NULL)
		{
			LogTrace(eERR, "connect database[" + dbid + "] failed");

			stdx::Throw(XG_SYSERR, "connect database failed");
		}

		DBConnectPool* pool = func(dbid.c_str());

		if (pool == NULL)
		{
			LogTrace(eERR, "connect database[" + dbid + "] failed");

			stdx::Throw(XG_SYSERR, "connect database failed");
		}

		sp<DBConnect> conn = pool->get();
		
		if (!conn)
		{
			LogTrace(eERR, "grasp database[" + dbid + "] connection failed");

			stdx::Throw(XG_SYSBUSY, "connect database failed");
		}

		return conn;
	}
}

///////////////////////////////////////////////////////////
#endif
