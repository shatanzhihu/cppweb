## 功能说明
1. 作者初衷是编写一个web框架支持C++开发cgi程序，于是cppweb诞生了。
2. 作者希望cppweb是一个大一统的框架，即可用于传统服务端编程也可用于web编程，作者希望http协议能做的更多，框架包括以下两个核心服务：
```
webserver：业务服务容器，通过配置也可升级为服务注册中心与定时任务调度中心。
webrouter：接口路由网关服务，对外提供统一的流量入口，主要负责请求分发以及黑白名称配置。
```
3. cppweb在读数据采用epoll网络模型，以任务队列的方式处理具体请求，回包也在任务队列中处理，理论上cppweb可支持单机10000个以上的并发连接。
4. cppweb易拓展，作者开发Java、Python等模块，用于支持Java、Python等语言开发cgi程序，开发者可以直接使用C/C++、Java、Python等语言进行混合开发。
5. cppweb追求小而巧，对于开源库是拿来即用，源码工程自带zlib、sqlite等源码代码，开发者无需另外下载，再此感谢zlib、sqlite等开源库的作者与开发团队。
6. 基于cppweb的微服务集群框架如下图所示，图中<green>绿色</green>部分包括服务注册中心与业务服务集群由webserver服务构成；图中<red>红色</red>部分包括外部接口网关与内部接口网关由webrouter接口路由网关服务构成。关于cppweb的更多内容请可访问[https://www.winfengtech.com/cppweb](https://www.winfengtech.com/cppweb)查看。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0905/112334_5db2c0e0_392413.png "notefile.png")

## 测试数据
1. cppweb在普通PC机(4核8G)上至少可支持每秒10000笔请求。
2. cppweb在1核1G的低配centos系统上至少支持每秒3000笔请求。
3. 下图是cppweb自身的流量监控数据：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0905/112355_2fce47b9_392413.png "notefile (1).png")

## 安装编译
1. 执行以下命令[下载源码](https://gitee.com/xungen/cppweb)。
```
git clone https://gitee.com/xungen/cppweb.git
```
2. 进入源码目录执行`source configure`命令，为使环境变量在当前会话中生效必须使用`source`命令执行`configure`进行编译配置。命令输出结果如下：
```
initialize configure
---------------------------------------------
1.check openssl success
2.check g++ compiler success
3.check java compiler success
4.create product directory success
5.export environment variable success
---------------------------------------------
>>> initialize build-essential success
```
3. 在源码目录下执行`make`命令，正常情况3~5分钟完成编译。
4. 执行`webserver -init $SOURCE_HOME/webapp/etc`命令初始化配置，命令执行成功后会在`$SOURCE_HOME/webapp/etc`目录下生成以下文件：
```
sqlite.db：基础数据文件
config.lua：启动配置文件
dbconfig.lua：数据库配置文件
mimeconfig.lua：MIME类型映射文件
```
5. 初始化完成后执行`strsvr`命令便可启动webserver服务。
6. 用浏览器打开`http://localhost:8888`地址进入webserver管理中心，如果webserver不是部署在本机需要将地址中的`localhost`替换为webserver所在服务器的IP地址，登陆用户与初始密码都为`system`。
