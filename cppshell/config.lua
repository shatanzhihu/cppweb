-- compiler
------------------------------------------------------------------
CC = "g++ -O2 -std=c++11"

-- compile setting
------------------------------------------------------------------
FLAG = "-DXG_WITHSSL -I/java/include -I/java/include/linux -I. -I./inc -I$SOURCE_HOME/library -I$SOURCE_HOME/library/zlib -I$PRODUCT_HOME/lib/openssl/inc -I$PRODUCT_HOME/lib/python/inc"

-- library link setting
------------------------------------------------------------------
LIB_LINK = "-L$PRODUCT_HOME/lib -ldbx.sqlite -lhttp -ldbx.base -lopenssl -lsqlite -ljson -lstdx -lclib -lzlib -L$PRODUCT_HOME/lib/openssl/lib -lssl -lcrypto"
