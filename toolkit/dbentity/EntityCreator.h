#ifndef XG_ENTITYCREATOR_H
#define XG_ENTITYCREATOR_H
///////////////////////////////////////////////////////////////////////
#include <regex>
#include <dbx/DBConnect.h>

class EntityCreator
{
protected:
	string head_name;
	string class_name;
	const string tab_name;
	vector<ColumnData>& vec;
	vector<string>& pkey_vec;

public:
	EntityCreator(vector<ColumnData>& vColumnData, vector<string>& vPKeys, const string& name);
	~EntityCreator();
	inline string getFileName()
	{
		string name = tab_name;
		return stdx::toupper(name);
	}
	inline const string& getClassName()
	{
		return class_name = "C" + getFileName();
	}
	bool create(const string& path);
	string getHeaderFilePath(const string& path);
	string getSourceFilePath(const string& path);
	const ColumnData& getPrimaryKeyData(const char* key_name);
	bool createHeaderFile(const string& filename);
	bool createSourceFile(const string& filename);
	bool writeHeader(TextFile& _out);
	bool writeSourceHeader(TextFile& _out);
	bool writeFindSource(TextFile& _out);
	bool writeNextSource(TextFile& _out);
	bool writeUpdateSource(TextFile& _out, bool flag = false);
	bool writeInsertSource(TextFile& _out);
	bool writeRemoveSource(TextFile& _out);
	bool writeClearSource(TextFile& _out);
	bool writeInitSource(TextFile& _out);
	bool writeCloseSource(TextFile& _out);
	bool writeGetValueSource(TextFile& _out);
	bool writeSetValueSource(TextFile& _out);
	
	inline static string GetTableQuerySQL(const string& tab_name)
	{
		return "SELECT * FROM " + tab_name + " WHERE 1=0";
	}

	static int ExportEntity(DBConnect* db, const string& nmregex, const string& srcpath);
	static int GetTableColumnData(DBConnect* conn, vector<ColumnData>& vec, const string& tab_name);
	static bool LoadConfig(const ConfigFile& file, DBConnectConfig& addr, string& nmregex, string& srcpath);
};
///////////////////////////////////////////////////////////////////////
#endif

